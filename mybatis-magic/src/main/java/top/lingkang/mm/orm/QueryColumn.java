package top.lingkang.mm.orm;

import top.lingkang.mm.error.MagicException;

import java.util.Collection;
import java.util.Map;

/**
 * @author lingkang
 * Created by 2024/3/20
 */
public class QueryColumn extends BaseQuery  implements BaseQueryInterface<QueryColumn>{
    private final Class<?> resultClass;
    private String columns = "";

    /**
     * 例如：
     * <pre>
     * {@code
     *     QueryColumn queryColumn = new QueryColumn(UserEntity.class, "id", "url", "create_time as createTime")
     *                 .orderByDesc("create_time");
     * }
     * </pre>
     *
     * @param resultClass 返回的结果类
     * @param column      需要查询的列，为空时，将用 * 替代：select * from table
     */
    public QueryColumn(Class<?> resultClass, String... column) {
        if (resultClass == null)
            throw new MagicException("结果类不能为空");
        this.resultClass = resultClass;
        if (column == null || column.length == 0) {
            columns = "*";
            return;
        }

        int pre = column.length - 1;
        for (int i = 0; i < column.length; i++) {
            columns += column[i];
            if (i < pre)
                columns += ",";
        }
    }

    public String getColumns() {
        return columns;
    }

    public Class<?> getResultClass() {
        return resultClass;
    }

    // override -----------------------------------------------------------------------------------------


    /**
     * 自定义操作符<br/>
     * 例如 sort_number > 1 ； sort_number = 2 ; sort_number > 3 ; sort_number >= 1; <br/>
     * 例如 sort_number is null ； sort_number = 2 ; sort_number > 3 ; sort_number >= 1; <br/>
     * <pre>
     * {@code
     * String id = userMapper.selectColumnOne(
     *     new QueryColumn(String.class, "id").symbol("id", "=", 2)
     * );
     * log.info("selectColumnOne: {}", id);
     * }
     * </pre>
     *
     * @since 1.1.0
     */
    @Override
    public QueryColumn symbol(String column, String symbol, Object value) {
        super.symbol(column, symbol, value);
        return this;
    }

    @Override
    public QueryColumn eq(String column, Object value) {
        super.eq(column, value);
        return this;
    }

    @Override
    public QueryColumn ne(String column, Object value) {
        super.ne(column, value);
        return this;
    }

    @Override
    public QueryColumn gt(String column, Object value) {
        super.gt(column, value);
        return this;
    }

    @Override
    public QueryColumn ge(String column, Object value) {
        super.ge(column, value);
        return this;
    }

    @Override
    public QueryColumn lt(String column, Object value) {
        super.lt(column, value);
        return this;
    }

    @Override
    public QueryColumn le(String column, Object value) {
        super.le(column, value);
        return this;
    }

    @Override
    public QueryColumn like(String column, Object value) {
        super.like(column, value);
        return this;
    }

    @Override
    public QueryColumn notLike(String column, Object value) {
        super.notLike(column, value);
        return this;
    }

    @Override
    public QueryColumn likeLeft(String column, Object value) {
        super.likeLeft(column, value);
        return this;
    }

    @Override
    public QueryColumn likeRight(String column, Object value) {
        super.likeRight(column, value);
        return this;
    }

    @Override
    public QueryColumn notLikeLeft(String column, Object value) {
        super.notLikeLeft(column, value);
        return this;
    }

    @Override
    public QueryColumn notLikeRight(String column, Object value) {
        super.notLikeRight(column, value);
        return this;
    }

    @Override
    public QueryColumn isNull(String column) {
        super.isNull(column);
        return this;
    }

    @Override
    public QueryColumn isNotNull(String column) {
        super.isNotNull(column);
        return this;
    }

    @Override
    public QueryColumn in(String column, Collection value) {
        super.in(column, value);
        return this;
    }

    @Override
    public QueryColumn notIn(String column, Collection value) {
        super.notIn(column, value);
        return this;
    }

    @Override
    public QueryColumn orderByAsc(String... ascColumn) {
        super.orderByAsc(ascColumn);
        return this;
    }

    @Override
    public QueryColumn orderByDesc(String... descColumn) {
        super.orderByDesc(descColumn);
        return this;
    }

    @Override
    public QueryColumn or() {
        super.or();
        return this;
    }

    @Override
    public QueryColumn sql(String sql) {
        super.sql(sql);
        return this;
    }

    @Override
    public QueryColumn sql(String sql, Map<String, Object> param) {
        super.sql(sql, param);
        return this;
    }

    protected void addQueryParam(StringBuilder sql, String p) {
        sql.append("#{").append(BaseMapperSql.param_q2).append(".param.").append(p).append("}");
    }
}
