package top.lingkang.mm.orm;

import lombok.Getter;
import lombok.ToString;

/**
 * @author lingkang
 * @create by 2024/3/12 16:17
 */
@Getter
@ToString
class ConditionValue {
    private Condition condition;
    private String column;
    private Object value;
    private String symbol;

    public ConditionValue(Condition condition, String column, Object value) {
        this.condition = condition;
        this.column = column;
        this.value = value;
    }

    public ConditionValue(String column, Object value) {
        this.column = column;
        this.value = value;
    }

    public ConditionValue(String column) {
        this.column = column;
    }

    public ConditionValue(Condition condition) {
        this.condition = condition;
    }

    public ConditionValue setSymbol(String symbol) {
        this.symbol = symbol;
        return this;
    }
}
