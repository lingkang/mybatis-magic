package top.lingkang.mm.orm;

import top.lingkang.mm.error.MagicException;

import java.util.Collection;
import java.util.Map;

/**
 * @author lingkang
 * Create by 2024/12/14 1:24
 * @since 1.1.0
 */
class QueryUtils {

    public static void initHandler(Map<Condition, ConditionHandler> handlers) {
        handlers.put(Condition.symbol, (sql, query, condition, p) -> {
            sql.append(condition.getColumn()).append(" ").append(condition.getSymbol()).append(" ");
            query.addQueryParam(sql, p);
            query.param.put(p, condition.getValue());
        });
        handlers.put(Condition.eq, (sql, query, condition, p) -> {
            sql.append(condition.getColumn()).append("= ");
            query.addQueryParam(sql, p);
            query.param.put(p, condition.getValue());
        });
        handlers.put(Condition.ne, (sql, query, condition, p) -> {
            sql.append(condition.getColumn()).append("<> ");
            query.addQueryParam(sql, p);
            query.param.put(p, condition.getValue());
        });
        handlers.put(Condition.gt, (sql, query, condition, p) -> {
            sql.append(condition.getColumn()).append("> ");
            query.addQueryParam(sql, p);
            query.param.put(p, condition.getValue());
        });
        handlers.put(Condition.ge, (sql, query, condition, p) -> {
            sql.append(condition.getColumn()).append(">= ");
            query.addQueryParam(sql, p);
            query.param.put(p, condition.getValue());
        });
        handlers.put(Condition.lt, (sql, query, condition, p) -> {
            sql.append(condition.getColumn()).append("< ");
            query.addQueryParam(sql, p);
            query.param.put(p, condition.getValue());
        });
        handlers.put(Condition.le, (sql, query, condition, p) -> {
            sql.append(condition.getColumn()).append("<= ");
            query.addQueryParam(sql, p);
            query.param.put(p, condition.getValue());
        });
        handlers.put(Condition.like, (sql, query, condition, p) -> {
            sql.append(condition.getColumn()).append(" like ");
            query.addQueryParam(sql, p);
            query.param.put(p, "%" + condition.getValue() + "%");
        });
        handlers.put(Condition.notLike, (sql, query, condition, p) -> {
            sql.append(condition.getColumn()).append(" not like ");
            query.addQueryParam(sql, p);
            query.param.put(p, "%" + condition.getValue() + "%");
        });
        handlers.put(Condition.likeLeft, (sql, query, condition, p) -> {
            sql.append(condition.getColumn()).append(" like ");
            query.addQueryParam(sql, p);
            query.param.put(p, "%" + condition.getValue());
        });
        handlers.put(Condition.likeRight, (sql, query, condition, p) -> {
            sql.append(condition.getColumn()).append(" like ");
            query.addQueryParam(sql, p);
            query.param.put(p, condition.getValue() + "%");
        });
        handlers.put(Condition.notLikeLeft, (sql, query, condition, p) -> {
            sql.append(condition.getColumn()).append(" not like ");
            query.addQueryParam(sql, p);
            query.param.put(p, "%" + condition.getValue());
        });
        handlers.put(Condition.notLikeRight, (sql, query, condition, p) -> {
            sql.append(condition.getColumn()).append(" not like ");
            query.addQueryParam(sql, p);
            query.param.put(p, condition.getValue() + "%");
        });
        handlers.put(Condition.isNull, (sql, query, condition, p) -> sql.append(condition.getColumn()).append(" is null"));
        handlers.put(Condition.isNotNull, (sql, query, condition, p) -> {
            sql.append(condition.getColumn()).append(" is not null");
        });

        handlers.put(Condition.in, (sql, query, condition, p) -> {
            if (condition.getValue() != null) {
                Collection collection = (Collection) condition.getValue();
                if (collection.isEmpty())
                    throw new MagicException("查询条件: in 入参不能为空！");
                sql.append(condition.getColumn()).append(" in (");
                int j = 1;
                for (Object o : collection) {
                    String name = p + "_" + j;
                    query.addQueryParam(sql, name);
                    query.param.put(name, o);
                    if (j < collection.size())
                        sql.append(",");
                    j++;
                }
                sql.append(")");
            } else
                throw new MagicException("in 入参不能为空！");
        });
        handlers.put(Condition.notIn, (sql, query, condition, p) -> {
            if (condition.getValue() != null) {
                Collection collection = (Collection) condition.getValue();
                if (collection.isEmpty())
                    throw new MagicException("查询条件: notIn 入参不能为空！");
                sql.append(condition.getColumn()).append(" not in (");
                int j = 1;
                for (Object o : collection) {
                    String name = p + "_" + j;
                    query.addQueryParam(sql, name);
                    query.param.put(name, o);
                    if (j < collection.size())
                        sql.append(",");
                    j++;
                }
                sql.append(")");
            } else
                throw new MagicException("in 入参不能为空！");
        });
    }

}
