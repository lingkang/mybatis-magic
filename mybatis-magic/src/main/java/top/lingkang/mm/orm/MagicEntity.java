package top.lingkang.mm.orm;

import lombok.Data;
import top.lingkang.mm.annotation.Id;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * 实体对象信息
 *
 * @Author lingkang
 * @Date 2024/3/1 10:28
 */
@Data
public class MagicEntity {
    private String tableName;
    private Class<?> clazz;
    // id 所在列的索引
    private int idIndex = -1;
    private Id idAnn;
    private List<Field> fields = new ArrayList<>();// 如果是public提取设置 field.setAccessible(true);
    private List<String> columnName = new ArrayList<>();
    private List<Method> postUpdate;
    private List<Method> preUpdate;
    private List<Field> autoCreateTime;
    private List<Field> autoUpdateTime;
    private List<String> autoUpdateTimeColumn;

    // 查询列，例如：select id, username, password from t_user
    private String selectTableSql;
}
