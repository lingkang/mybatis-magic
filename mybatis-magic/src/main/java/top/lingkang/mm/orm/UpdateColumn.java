package top.lingkang.mm.orm;

import top.lingkang.mm.error.MagicException;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author lingkang
 * Create by 2024/12/1 1:57
 */
public class UpdateColumn extends BaseQuery implements BaseQueryInterface<UpdateColumn> {
    protected HashMap<String, Object> setParam = new HashMap<>();

    /**
     * 指定更新列
     *
     * @param column 需要更新的列
     * @param value  需要更新的值
     */
    public UpdateColumn set(String column, Object value) {
        // 参数为 mm_列名
        setParam.put("mm_" + column, value);
        return this;
    }

    // mybatis的底层将会调用获取SQL
    @Override
    public String getSql() {
        Set<String> set = setParam.keySet();
        if (set.isEmpty())
            throw new MagicException("QueryUpdate 不能更新空列，请调用 set(column,value) 设置要更新的列");

        String buildSql = buildSql();
        if (buildSql.isEmpty())
            throw new MagicException("QueryUpdate 必须添加条件，否则全量更新请使用 MapperManage#createUpdate 进行更新全部数据（安全：限制全量更新） not allowed to update all");

        StringBuilder sb = new StringBuilder();
        for (String column : set) {
            sb.append(column.substring(3)).append("=");
            addQueryParam(sb, column);
            sb.append(", ");
        }
        sb.deleteCharAt(sb.length() - 2);
        // 设置上参数
        param.putAll(setParam);
        return sb.append(" where ") + buildSql.substring(4);
    }


    // override -----------------------------------------------------------------------------------------


    /**
     * 自定义操作符<br/>
     * 例如 sort_number > 1 ； sort_number = 2 ; sort_number > 3 ; sort_number >= 1; <br/>
     * 例如 sort_number is null ； sort_number = 2 ; sort_number > 3 ; sort_number >= 1; <br/>
     * <pre>
     * {@code
     * int result = userMapper.updateByColumn(
     *     new UpdateColumn().set("id", 2).symbol("id", "=", 1)
     * );
     * log.info("updateByColumn: {}", result);
     * }
     * </pre>
     *
     * @since 1.1.0
     */
    @Override
    public UpdateColumn symbol(String column, String symbol, Object value) {
        super.symbol(column, symbol, value);
        return this;
    }

    @Override
    public UpdateColumn eq(String column, Object value) {
        super.eq(column, value);
        return this;
    }

    @Override
    public UpdateColumn ne(String column, Object value) {
        super.ne(column, value);
        return this;
    }

    @Override
    public UpdateColumn gt(String column, Object value) {
        super.gt(column, value);
        return this;
    }

    @Override
    public UpdateColumn ge(String column, Object value) {
        super.ge(column, value);
        return this;
    }

    @Override
    public UpdateColumn lt(String column, Object value) {
        super.lt(column, value);
        return this;
    }

    @Override
    public UpdateColumn le(String column, Object value) {
        super.le(column, value);
        return this;
    }

    @Override
    public UpdateColumn like(String column, Object value) {
        super.like(column, value);
        return this;
    }

    @Override
    public UpdateColumn notLike(String column, Object value) {
        super.notLike(column, value);
        return this;
    }

    @Override
    public UpdateColumn likeLeft(String column, Object value) {
        super.likeLeft(column, value);
        return this;
    }

    @Override
    public UpdateColumn likeRight(String column, Object value) {
        super.likeRight(column, value);
        return this;
    }

    @Override
    public UpdateColumn notLikeLeft(String column, Object value) {
        super.notLikeLeft(column, value);
        return this;
    }

    @Override
    public UpdateColumn notLikeRight(String column, Object value) {
        super.notLikeRight(column, value);
        return this;
    }

    @Override
    public UpdateColumn isNull(String column) {
        super.isNull(column);
        return this;
    }

    @Override
    public UpdateColumn isNotNull(String column) {
        super.isNotNull(column);
        return this;
    }

    @Override
    public UpdateColumn in(String column, Collection value) {
        super.in(column, value);
        return this;
    }

    @Override
    public UpdateColumn notIn(String column, Collection value) {
        super.notIn(column, value);
        return this;
    }

    /**
     * 更新操作不可用排序
     */
    @Override
    public UpdateColumn orderByAsc(String... ascColumn) {
        throw new MagicException("更新操作不能使用排序");
    }

    /**
     * 更新操作不可用排序
     */
    @Override
    public UpdateColumn orderByDesc(String... descColumn) {
        throw new MagicException("更新操作不能使用排序");
    }

    @Override
    public UpdateColumn or() {
        super.or();
        return this;
    }

    @Override
    public UpdateColumn sql(String sql) {
        super.sql(sql);
        return this;
    }

    @Override
    public UpdateColumn sql(String sql, Map<String, Object> param) {
        super.sql(sql, param);
        return this;
    }

    protected void addQueryParam(StringBuilder sql, String p) {
        sql.append("#{").append(BaseMapperSql.param_q2).append(".param.").append(p).append("}");
    }
}
