package top.lingkang.mm.orm;

/**
 * @author lingkang
 * Create by 2024/12/14 1:19
 * @since 1.1.0
 */
interface ConditionHandler {
    /**
     * 聚合处理
     */
    void handle(StringBuilder sql, BaseQuery query, ConditionValue condition, String p);
}
