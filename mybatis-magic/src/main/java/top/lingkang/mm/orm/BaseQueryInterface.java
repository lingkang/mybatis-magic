package top.lingkang.mm.orm;

import java.util.Collection;
import java.util.Map;

/**
 * @author lingkang
 * Create by 2025/1/8 18:17
 * @since 1.1.1
 */
interface BaseQueryInterface<Q> {
    Q symbol(String column, String symbol, Object value);

    Q eq(String column, Object value);

    Q ne(String column, Object value);

    Q gt(String column, Object value);

    Q ge(String column, Object value);

    Q lt(String column, Object value);

    Q le(String column, Object value);

    Q like(String column, Object value);

    Q notLike(String column, Object value);

    Q likeLeft(String column, Object value);

    Q likeRight(String column, Object value);

    Q notLikeLeft(String column, Object value);

    Q notLikeRight(String column, Object value);

    Q isNull(String column);

    Q isNotNull(String column);

    Q in(String column, Collection value);

    Q notIn(String column, Collection value);

    Q orderByAsc(String... ascColumn);

    Q orderByDesc(String... descColumn);

    Q or();

    Q sql(String sql);

    Q sql(String sql, Map<String, Object> param);

    String buildSql();

    String getSql();
}
