package top.lingkang.mm.orm;

import org.apache.ibatis.annotations.Lang;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

/**
 * 自定义sql处理
 *
 * @author lingkang
 * @create by 2024/3/6 10:21
 */
public interface MagicCreateLangMapper<T> {
    @Select("${execSql_}")
    @Lang(MagicCreateLangDriver.class)
    List<T> list(Map<String, Object> map);

    @Select("${execSql_}")
    @Lang(MagicCreateLangDriver.class)
    T one(Map<String, Object> map);

    @Update("${execSql_}")
    @Lang(MagicCreateLangDriver.class)
    int update(Map<String, Object> map);
}
