package top.lingkang.mm.orm;

import java.lang.reflect.Field;
import java.sql.Date;

/**
 * @author lingkang
 * Create by 2024/11/18 8:39
 * @since 1.1.0
 */
public class SetTimeDateSql implements SetTime {
    @Override
    public void set(Field field, Object obj) throws IllegalAccessException {
        field.set(obj, new Date(System.currentTimeMillis()));
    }
}
