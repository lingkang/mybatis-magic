package top.lingkang.mm.orm;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author lingkang
 * @create by 2024/3/12 11:20
 */
class BaseMapperSql {
    public static final String selectAll = "magic_base_selectAll";
    public static final String createQuery = "magic_base_createQuery";
    public static final String selectById = "magic_base_selectById";
    public static final String selectInIds = "magic_base_selectInIds";
    public static final String selectByQuery = "magic_base_selectByQuery";
    public static final String selectByQueryOne = "magic_base_selectByQueryOne";
    public static final String selectCount = "magic_base_selectCount";
    public static final String selectCountByQuery = "magic_base_selectCountByQuery";
    public static final String selectColumn = "magic_base_selectColumn";
    public static final String selectColumnOne = "magic_base_selectColumnOne";
    public static final String existsById = "magic_base_existsById";
    public static final String existsByEntity = "magic_base_existsByEntity";
    public static final String existsByQuery = "magic_base_existsByQuery";
    public static final String insert = "magic_base_insert";
    public static final String insertBatch = "magic_base_insertBatch";

    public static final String updateById = "magic_base_updateById";
    public static final String updateByQuery = "magic_base_updateByQuery";
    public static final String updateByColumn = "magic_base_updateByColumn";
    public static final String deleteById = "magic_base_deleteById";
    public static final String deleteInIds = "magic_base_deleteInIds";
    public static final String deleteByQuery = "magic_base_deleteByQuery";

    private static final Set<String> set;

    static {
        List<String> list = Arrays.asList(
                selectAll, createQuery, selectById, selectByQuery, selectCount, selectCountByQuery,
                selectColumn, selectColumnOne, existsById, existsByQuery, insert, insertBatch, updateById, selectByQueryOne,
                updateByQuery, deleteById, deleteByQuery, updateByColumn, existsByEntity, selectInIds, deleteInIds
        );
        set = new HashSet<>(list);
    }

    public static boolean isBaseMapperSql(String sql) {
        if (sql == null)
            return false;
        return set.contains(sql);
    }


    public static final String magic_base_e = "e";
    public static final String magic_base_list = "list";
    public static final String param_q = "q";// Query
    public static final String param_q2 = "q2";// QueryColumn
    public static final String param_id = "id";
    public static final String param_id_2 = "id2";
    public static final String param_id_3 = "id3";
}
