package top.lingkang.mm.orm;

import java.lang.reflect.Field;

/**
 * @author lingkang
 * Create by 2024/11/18 8:38
 * @since 1.1.0
 */
interface SetTime {
    void set(Field field, Object obj) throws IllegalAccessException;
}
