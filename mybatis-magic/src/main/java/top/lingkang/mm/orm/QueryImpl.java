package top.lingkang.mm.orm;

import java.util.Collection;
import java.util.Map;

/**
 * 条件请参考类：{@link Condition}
 *
 * @author lingkang
 * Create by 2024/12/1 2:30
 */
public interface QueryImpl {
    QueryImpl eq(String column, Object value);

    QueryImpl ne(String column, Object value);

    QueryImpl gt(String column, Object value);

    QueryImpl ge(String column, Object value);

    QueryImpl lt(String column, Object value);

    QueryImpl le(String column, Object value);

    QueryImpl like(String column, Object value);

    QueryImpl notLike(String column, Object value);

    QueryImpl likeLeft(String column, Object value);

    QueryImpl likeRight(String column, Object value);

    QueryImpl notLikeLeft(String column, Object value);

    QueryImpl notLikeRight(String column, Object value);

    QueryImpl isNull(String column, Object value);

    QueryImpl isNotNull(String column, Object value);

    QueryImpl in(String column, Collection value);

    QueryImpl notIn(String column, Collection value);

    QueryImpl orderByAsc(String... ascColumn);

    QueryImpl orderByDesc(String... descColumn);

    QueryImpl or();

    QueryImpl sql(String sql);

    QueryImpl sql(String sql, Map<String, Object> param);
}
