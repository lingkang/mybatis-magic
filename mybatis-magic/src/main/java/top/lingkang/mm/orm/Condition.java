package top.lingkang.mm.orm;

import lombok.Getter;
import lombok.ToString;

/**
 * @author lingkang
 * @create by 2024/3/12 16:17
 */
@Getter
@ToString
public enum Condition {
    // 操作符号 例如 new Query.symbol("column", ">", 1);
    // @since 1.1.0
    symbol,
    // 等于 =
    eq,
    // 不等于 <>
    ne,
    // 大于 >
    gt,

    // 大于等于 >=
    ge,
    // 小于 <
    lt,
    // 小于等于 <=
    le,
    // LIKE '%值%'
    like,
    // NOT LIKE '%值%'
    notLike,
    // LIKE '%值'
    likeLeft,
    // LIKE '值%'
    likeRight,
    // NOT LIKE '%值'
    notLikeLeft,
    // NOT LIKE '值%'
    notLikeRight,
    // 字段 IS NULL
    isNull,
    // 字段 IS NOT NULL
    isNotNull,
    // 字段 IN (value.get(0), value.get(1), ...)
    in,
    // 字段 NOT IN (value.get(0), value.get(1), ...)
    notIn,
    // 排序：ORDER BY 字段, ... ASC
    orderByAsc,
    // 排序：ORDER BY 字段, ... DESC
    orderByDesc,
    // 拼接 OR
    or,
    // 默认 and
    and,
    // 自定义的sql
    sql,
    ;
    private String column;
    private Object values;

    private Condition() {
    }

    Condition(String column, Object values) {
        this.column = column;
        this.values = values;
    }
}
