package top.lingkang.mm.orm;

import lombok.Getter;
import lombok.Setter;

/**
 * @author lingkang
 * @create by 2024/3/12 18:08
 */
@Setter
@Getter
class ConditionOrderBy {
    private Condition by;
    private String[] columns;

    public ConditionOrderBy(Condition by, String[] columns) {
        this.by = by;
        this.columns = columns;
    }
}
