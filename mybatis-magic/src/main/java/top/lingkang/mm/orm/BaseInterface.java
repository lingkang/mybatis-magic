package top.lingkang.mm.orm;

/**
 * @author lingkang
 * @create by 2024/3/12 10:03
 */
class BaseInterface {
    private String interfaceStr;
    private Class<?> inter;
    private Class<?> entityClass;

    public String getInterfaceStr() {
        return interfaceStr;
    }

    public void setInterfaceStr(String interfaceStr) {
        this.interfaceStr = interfaceStr;
    }

    public Class<?> getInter() {
        return inter;
    }

    public void setInter(Class<?> inter) {
        this.inter = inter;
    }

    public Class<?> getEntityClass() {
        return entityClass;
    }

    public void setEntityClass(Class<?> entityClass) {
        this.entityClass = entityClass;
    }
}
