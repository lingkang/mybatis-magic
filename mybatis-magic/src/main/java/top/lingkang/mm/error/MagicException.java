package top.lingkang.mm.error;

/**
 * @Author lingkang
 * @Date 2024/2/29 16:50
 */
public class MagicException extends RuntimeException{
    public MagicException() {
    }

    public MagicException(String message) {
        super(message);
    }

    public MagicException(String message, Throwable cause) {
        super(message, cause);
    }

    public MagicException(Throwable cause) {
        super(cause);
    }

    public MagicException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
