package top.lingkang.mm.annotation;

import java.lang.annotation.*;

/**
 * 会在插入数据、更新数据时，自动添加创建时间。 <br/>
 * 只适用于实体类对象的ORM操作，自己编写的sql无法影响到 <br/>
 * 适用于时间类型属性的数据、或者long型时间戳、String类型将会设置为当前时间戳，其他类型报错 <br/>
 * 无论是否为空，都会设置最新的时间 <br/>
 * 其他推荐: {@link AutoCreateTime}
 *
 * @author lingkang
 * Create by 2024/11/18 8:14
 * @since 1.1.0
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AutoUpdateTime {
}
