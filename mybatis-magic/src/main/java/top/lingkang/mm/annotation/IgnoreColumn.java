package top.lingkang.mm.annotation;

import java.lang.annotation.*;

/**
 * 转换sql语句、实体属性映射表字段时，将忽略此属性字段。
 *
 * @Author lingkang
 * @Date 2024/3/1 17:25
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface IgnoreColumn {
}
