package top.lingkang.mm.annotation;

import java.lang.annotation.*;

/**
 * 表映射实体注解
 * <pre>{@code
 * @Table("t_user")
 * public class User{
 *     // ...
 * }
 * }
 * </pre>
 * <p>
 *
 * @Author lingkang
 * @Date 2024/3/1
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Table {
    /**
     * 对应数据库的名称
     */
    String value() default "";
}
