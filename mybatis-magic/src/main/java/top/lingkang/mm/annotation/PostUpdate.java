package top.lingkang.mm.annotation;

import java.lang.annotation.*;

/**
 * 执行更新后执行此注解下的方法，无入参。适用于审计<br/>
 * 但是在 {@link top.lingkang.mm.orm.BaseMapper} 接口调用无效<br/>
 * 只会在调用下面的方法后回调：<br/>
 * <pre>{@code {
 *     mapperManage.insert(entity);
 *     mapperManage.updateById(entity);
 * }}
 * </pre>
 *
 * @author lingkang
 * Created by 2024/3/2
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface PostUpdate {
}
