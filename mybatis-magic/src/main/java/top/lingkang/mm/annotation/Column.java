package top.lingkang.mm.annotation;

import cn.hutool.core.text.NamingCase;

import java.lang.annotation.*;

/**
 * 实体属性注解，列名称
 *
 * @Author lingkang
 * @Date 2024/3/1 16:27
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Column {
    /**
     * 数据库的列名称
     */
    String value() default "";

    /**
     * 自动驼峰转换，转换后的名称将会映射到数据库的列 <br/>
     * 具体转换细节：{@link NamingCase#toUnderlineCase(CharSequence)}
     * <pre>
     * {@code
     *     createTime --> create_time
     *     UpdateTime --> update_time
     *     is_id --> is_id
     *     Is_Index --> is_index
     * }
     * </pre>
     *
     * @since 1.1.0
     */
    boolean toUnderlineCase() default false;
}
