package top.lingkang.mm.annotation;

import java.lang.annotation.*;

/**
 * 用于扫描加载 mapper 接口，例如
 * <pre>
 * {@code
 *   @MagicMapper
 *   public interface UserMapper {
 *       @Select("select * from t_user")
 *       List<UserEntity> all();
 *   }
 * }
 * </pre>
 *
 * @author lingkang
 * Created by 2024/3/6
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface MagicMapper {
}
