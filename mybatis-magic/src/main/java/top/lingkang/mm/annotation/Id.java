package top.lingkang.mm.annotation;

import top.lingkang.mm.constant.IdType;

import java.lang.annotation.*;

/**
 * 标记主键Id <br>
 *
 * @Author lingkang
 * @Date 2024/3/1 17:30
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Id {
    /**
     * id 的类型，如果为 AUTO 则要数据库支持
     */
    IdType value() default IdType.NODE;

    /**
     * id类型为 ASSIGN 时生效，自动生成{@link top.lingkang.mm.gen.IdGenerate} 的入参
     */
    String assignParam() default "";

    /**
     * id类型为 AUTO 时生效，需要数据库支持 sequence
     */
    String sequence() default "";
}
