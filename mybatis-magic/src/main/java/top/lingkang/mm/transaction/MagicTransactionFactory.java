package top.lingkang.mm.transaction;

import org.apache.ibatis.session.TransactionIsolationLevel;
import org.apache.ibatis.transaction.Transaction;
import org.apache.ibatis.transaction.TransactionFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.util.Properties;

/**
 * @Author lingkang
 * @Date 2024/2/29 13:58
 */
public class MagicTransactionFactory implements TransactionFactory {

    @Override
    public Transaction newTransaction(Connection conn) {
        throw new UnsupportedOperationException("未支持的事务创建，已经交由 mybatis-magic 管理");
    }

    @Override
    public Transaction newTransaction(DataSource dataSource, TransactionIsolationLevel level, boolean autoCommit) {
        return new MagicTransaction(dataSource);
    }

    @Override
    public void setProperties(Properties props) {
        // 不操作
    }
}
