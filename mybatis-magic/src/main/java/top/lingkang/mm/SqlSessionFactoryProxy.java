package top.lingkang.mm;

import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * SqlSessionFactory 代理
 *
 * @author lingkang
 * @create by 2024/3/4 16:29
 */
public class SqlSessionFactoryProxy implements InvocationHandler {

    private final SqlSessionFactory sqlSessionFactory;

    public SqlSessionFactoryProxy(SqlSessionFactory sqlSessionFactory) {
        this.sqlSessionFactory = sqlSessionFactory;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (method.getReturnType() == Configuration.class)
            return sqlSessionFactory.getConfiguration();
        MagicSqlSession sqlSession = new MagicSqlSession(sqlSessionFactory, null);
        return sqlSession;
    }
}
