package top.lingkang.mm.page;

import java.util.regex.Matcher;

/**
 * 使用OFFSET和FETCH NEXT子句来实现分页查询。这是SQL Server 2012之后引入的特性
 *
 * @author lingkang
 * @create by 2024/4/24 10:44
 */
public class PageSqlHandleSqlServer implements PageSqlHandle {
    @Override
    public PageSqlInfo handleSql(String selectSql, int page, int size) {
        String sql = selectSql.toLowerCase();

        int index = sql.indexOf("from");
        int start = sql.substring(0, index).indexOf("(");
        while (start != -1) {
            start = sql.indexOf("(", start + 1);
            if (start == -1)
                index = sql.indexOf("from", index + 1);
        }

        // 匹配排序，可能存在排序，需要特殊处理
        Matcher matcher = orderBy.matcher(sql);
        PageSqlInfo info = new PageSqlInfo();
        if (matcher.find())
            info.setCountSql("select count(*) " + selectSql.substring(index, matcher.start()));
        else
            info.setCountSql("select count(*) " + selectSql.substring(index));

        info.setSelectSql(selectSql + " offset " + (page - 1) * size + " rows fetch next " + size + " row only");
        return info;
    }
}
