package top.lingkang.mm.page;

import lombok.Data;

/**
 * 分页对象
 *
 * @author lingkang
 * Created by 2024/3/3
 */
@Data
public class PageInfo {
    private int page;
    private int size;
    private long total;
    // 是否完成了分页查询
    private boolean complete;

    /**
     * 获取总分页数
     */
    public int countPageNumber() {
        return (int) Math.ceil(total / (double) size);
    }
}
