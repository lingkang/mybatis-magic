package top.lingkang.mm.page;

import lombok.Data;

/**
 * @author lingkang
 * Created by 2024/3/3
 */
@Data
public class PageSqlInfo {
    // 分页查询统计个数的sql
    private String countSql;
    // 分页查询的sql
    private String selectSql;
}
