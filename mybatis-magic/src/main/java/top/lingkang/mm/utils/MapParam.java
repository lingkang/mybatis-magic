package top.lingkang.mm.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * map 参数
 *
 * @author lingkang
 * Create by 2024/12/10 16:29
 * @since 1.1.0
 */
public class MapParam extends HashMap<String, Object> {

    @Override
    public MapParam put(String key, Object value) {
        super.put(key, value);
        return this;
    }

    public MapParam add(String key, Object value) {
        super.put(key, value);
        return this;
    }

    /**
     * 添加 map 参数
     *
     * @param params map参数
     */
    public MapParam addParam(Map<String, Object> params) {
        super.putAll(params);
        return this;
    }

    public static MapParam create(String key, Object value) {
        return new MapParam().add(key, value);
    }

    public static MapParam create() {
        return new MapParam();
    }
}
