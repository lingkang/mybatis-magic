package top.lingkang.mm.utils;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * @author lingkang
 * Create by 2024/11/28 16:05
 */
public class CommonUtils {
    private static final Snowflake snowflake = IdUtil.getSnowflake(1, 1);
    private static final SimpleDateFormat yyyyMM = new SimpleDateFormat("yyyyMM");
    private static final Random random = new Random();
    private static final String digits = "0123456789abcdefghijklmnopqrstuvwxyz"; // 36进制字符集

    /**
     * 获取当前时间的 yyyyMM 例如 202411
     *
     * @return 202411
     */
    public static String getCurrentYyyyMM() {
        return yyyyMM.format(new Date());
    }


    /**
     * 返回数字随机数
     */
    public static String randomNumber(int length) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            sb.append(random.nextInt(10));
        }
        return sb.toString();
    }

    /**
     * 返回随机字母,a~z(小写)
     */
    public static String randomLetter(int length) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            sb.append((char) (97 + random.nextInt(26)));
        }
        return sb.toString();
    }

    /**
     * 生成一个雪花算法id
     *
     * @return 生成一个雪花算法id
     */
    public static long nextId() {
        return snowflake.nextId();
    }

    /**
     * 生成一个雪花算法id
     *
     * @return 生成一个雪花算法id
     */
    public static String nextIdStr() {
        return snowflake.nextIdStr();
    }

    /**
     * 雪花算法转36进制数，例如：dw814a9hjmyo
     * 消耗性能
     */
    public static String nextId36() {
        return to36(nextId());
    }

    /**
     * 十进制转36进制
     *
     * @param number 十进制数字
     * @return 36进制字符
     */
    public static String to36(long number) {
        StringBuilder result = new StringBuilder();
        while (number > 0) {
            int remainder = (int) (number % 36); // 计算余数
            result.insert(0, digits.charAt(remainder)); // 将余数转换为对应字符并插入到结果字符串的开头
            number /= 36; // 更新待转换的数
        }
        return result.toString();
    }

    /**
     * 移除字符串中所有空格
     *
     * @param str 字符串
     * @return 没有空格的字符串
     */
    public static String removeSpace(String str) {
        while (str.contains(" "))
            str = str.replace(" ", "");
        return str;
    }

    /**
     * 字符串转 Sha1
     *
     * @param input 字符串
     * @return sha1
     */
    public static String toSha1(String input) {
        try {
            MessageDigest sha1Digest = MessageDigest.getInstance("SHA-1");
            byte[] hashedBytes = sha1Digest.digest(input.getBytes());
            StringBuilder sb = new StringBuilder();
            for (byte b : hashedBytes) {
                sb.append(String.format("%02x", b));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("SHA-1 not supported", e);
        }
    }

    /**
     * 如果 num1 小于 num2，则返回 -1，表示 version1 小于 version2。
     * 如果 num1 大于 num2，则返回 1，表示 version1 大于 version2。
     * 如果所有部分都相等，则返回 0，表示两个版本号相等。
     */
    public static int compareVersion(String version1, String version2) {
        String[] parts1 = version1.split("\\.");
        String[] parts2 = version2.split("\\.");
        int maxLength = Math.max(parts1.length, parts2.length);
        for (int i = 0; i < maxLength; i++) {
            int num1 = i < parts1.length ? Integer.parseInt(parts1[i]) : 0;
            int num2 = i < parts2.length ? Integer.parseInt(parts2[i]) : 0;
            if (num1 < num2) {
                return -1;
            } else if (num1 > num2) {
                return 1;
            }
        }
        return 0;
    }

    /**
     * 判断当前环境是否是window系统
     *
     * @return
     */
    public static boolean isWindowSystem() {
        return System.getProperty("os.name").toLowerCase().contains("windows");
    }
}
