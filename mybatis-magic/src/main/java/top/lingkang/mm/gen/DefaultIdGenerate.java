package top.lingkang.mm.gen;


import top.lingkang.mm.utils.CommonUtils;

/**
 * 默认生成雪花算法转36进制字符串(12位长度)
 *
 * @author lingkang
 * @create by 2024/3/15 15:52
 * @since 1.1.0
 */
public class DefaultIdGenerate implements IdGenerate {
    public static final IdGenerate instance = new DefaultIdGenerate();

    @Override
    public Object nextId(String param) {
        return CommonUtils.nextId36();
    }
}
