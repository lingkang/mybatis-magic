package top.lingkang.mm.gen;

import top.lingkang.mm.utils.CommonUtils;

/**
 * 默认生成雪花算法转36进制字符串(12位长度)，{@link CommonUtils#nextId36()}<br/>
 * {@link DefaultIdGenerate}
 *
 * @author lingkang
 * Created by 2024/3/3
 * @since 1.1.0
 */
public interface IdGenerate {
    /**
     * @param param 分配id的入参
     */
    Object nextId(String param);
}
