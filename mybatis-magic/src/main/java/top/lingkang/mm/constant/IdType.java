package top.lingkang.mm.constant;

/**
 * @author lingkang
 * @create by 2024/3/1 15:39
 */
public enum IdType {
    /**
     * 默认id类型，标记 id 属性字段
     */
    NODE,
    /**
     * 分派id类型，当插入数据，id为空时，自动为id分配一个值: {@link top.lingkang.mm.gen.DefaultIdGenerate}
     */
    ASSIGN,
    /**
     * 数据库自动生成id类型，需要数据库支持
     */
    AUTO
}
