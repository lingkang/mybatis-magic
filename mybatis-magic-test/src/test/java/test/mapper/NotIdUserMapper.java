package test.mapper;

import top.lingkang.mm.orm.BaseMapper;

/**
 * @author lingkang
 * @create by 2024/3/13 16:42
 */
public interface NotIdUserMapper extends BaseMapper<NotIdUserEntity> {
}
