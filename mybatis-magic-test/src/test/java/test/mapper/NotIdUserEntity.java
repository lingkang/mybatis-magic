package test.mapper;

import lombok.Data;
import top.lingkang.mm.annotation.Table;

/**
 * @author lingkang
 * @create by 2024/3/13 16:41
 */
@Data
@Table("t_user")
public class NotIdUserEntity {
    private String id;
    private String username;
}
