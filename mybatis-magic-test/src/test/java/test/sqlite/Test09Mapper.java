package test.sqlite;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import test.all.AllTest21_Mapper;
import top.lingkang.mm.orm.MapperManageImpl;
import top.lingkang.mm.orm.Query;
import top.lingkang.mmt.entity.UserEntity;
import top.lingkang.mmt.mapper.UserMapper;
import utils.TestUtils;

import java.util.List;

/**
 * @author lingkang
 * Created by 2024/3/14
 */
@Slf4j
public class Test09Mapper extends AllTest21_Mapper {
    @BeforeEach
    public void init() {
        sqlSession = TestUtils.getSqlite();
        TestUtils.initScript("script/init-sqlite.sql", sqlSession);
        mapperManage = new MapperManageImpl(sqlSession.getConfiguration(), sqlSession);
    }

}
