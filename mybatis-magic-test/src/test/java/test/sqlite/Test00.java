package test.sqlite;

import org.apache.ibatis.session.SqlSession;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.TestUtils;
import top.lingkang.mm.orm.MapperManage;
import top.lingkang.mm.orm.MapperManageImpl;
import top.lingkang.mmt.mapper.UserMapper;

/**
 * @author lingkang
 * @create by 2024/3/11 17:31
 */
public class Test00 {
    protected SqlSession sqlSession;
    protected MapperManage mapperManage;

    @BeforeEach
    public void init() {
        sqlSession = TestUtils.getSqlite();
        TestUtils.initScript("script/init-sqlite.sql", sqlSession);
        mapperManage = new MapperManageImpl(sqlSession.getConfiguration(), sqlSession);
    }

    @Test
    public void test01(){
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        System.out.println(mapper.selectAll());
    }
}
