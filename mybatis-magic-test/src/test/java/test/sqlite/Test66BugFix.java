package test.sqlite;

import org.junit.jupiter.api.BeforeEach;
import test.all.Test99_Bug_Fix;
import top.lingkang.mm.orm.MapperManageImpl;
import utils.TestUtils;

/**
 * @author lingkang
 * Create by 2025/1/20 2:58
 */
public class Test66BugFix extends Test99_Bug_Fix {
    @BeforeEach
    public void init() {
        sqlSession = TestUtils.getSqlite();
        TestUtils.initScript("script/init-sqlite.sql", sqlSession);
        mapperManage = new MapperManageImpl(sqlSession.getConfiguration(), sqlSession);
    }


}
