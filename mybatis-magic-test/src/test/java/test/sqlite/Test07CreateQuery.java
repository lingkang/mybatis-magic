package test.sqlite;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import test.all.AllTest11_CreateQuery;
import top.lingkang.mm.orm.MapperManageImpl;
import top.lingkang.mm.orm.QueryColumn;
import top.lingkang.mm.page.PageHelper;
import top.lingkang.mm.page.PageInfo;
import top.lingkang.mmt.entity.UserEntity;
import top.lingkang.mmt.mapper.UserMapper;
import utils.TestUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lingkang
 * @create by 2024/3/11 14:48
 */
@Slf4j
public class Test07CreateQuery extends AllTest11_CreateQuery {
    @BeforeEach
    public void init() {
        sqlSession = TestUtils.getSqlite();
        TestUtils.initScript("script/init-sqlite.sql", sqlSession);
        mapperManage = new MapperManageImpl(sqlSession.getConfiguration(), sqlSession);
    }

    @Test
    public void queryOne() {
        Map<String, Object> map = new HashMap<>();
        map.put("id", 123);
        int execute = mapperManage.createUpdate("insert into t_user(id) values(#{id});")
                .addParam(map)
                .execute();
        System.out.println(execute);
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        QueryColumn queryColumn = new QueryColumn(UserEntity.class, "id", "username").orderByAsc("id");
        queryColumn.eq("id", 1);
        PageHelper.startPage(1, 10);
        System.out.println(mapper.selectColumn(queryColumn));
        PageInfo pageInfo = PageHelper.getPage();
        System.out.println(pageInfo);
    }

}
