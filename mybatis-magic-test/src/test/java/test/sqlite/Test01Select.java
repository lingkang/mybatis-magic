package test.sqlite;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import test.all.AllTest01_Select;
import top.lingkang.mm.orm.MapperManageImpl;
import top.lingkang.mm.page.PageHelper;
import top.lingkang.mm.page.PageInfo;
import top.lingkang.mmt.entity.UserEntity;
import top.lingkang.mmt.entity.UserEntityCore;
import top.lingkang.mmt.mapper.UserMapper;
import utils.TestUtils;

import java.util.Date;
import java.util.List;

/**
 * @author lingkang
 * @create by 2024/3/11 15:41
 */
@Slf4j
public class Test01Select extends AllTest01_Select {
    @BeforeEach
    public void init() {
        sqlSession = TestUtils.getSqlite();
        TestUtils.initScript("script/init-sqlite.sql", sqlSession);
        mapperManage = new MapperManageImpl(sqlSession.getConfiguration(), sqlSession);
    }

    @Test
    public void test01() throws Exception {
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        List<UserEntity> userEntities = mapper.selectAll();
        System.out.println(userEntities);
        mapper.selectAll();
        mapper.selectAll();
        mapper.selectAll();
        mapper.selectAll();
    }

    @Test
    public void test011() throws Exception {
        PageHelper.startPage(1, 10);
        List date = mapperManage.createQuery("select create_time from t_user where create_time is not null", Date.class).getList();
        PageInfo page = PageHelper.getPage();
        System.out.println(page.getTotal());
        System.out.println(date);
    }

    @Test
    public void test012() throws Exception {
        List<UserEntity> list = mapperManage.selectAll(UserEntity.class);
        System.out.println(list);
    }

    @Test
    public void test013() throws Exception {
        List<UserEntityCore> list = mapperManage.selectAll(UserEntityCore.class);
        System.out.println(list);
    }
}
