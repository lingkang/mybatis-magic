package test.sqlite;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.stone.beecp.BeeDataSource;
import org.stone.beecp.BeeDataSourceConfig;
import top.lingkang.mm.MagicConfiguration;
import top.lingkang.mm.MagicSqlSession;
import top.lingkang.mm.orm.MapperManage;
import top.lingkang.mm.orm.MapperManageImpl;
import top.lingkang.mmt.entity.UserEntityCore;
import utils.TestUtils;

import java.util.List;

/**
 * @author lingkang
 * Create by 2025/2/23 3:05
 */
@Slf4j
public class Test10Core {
    private SqlSession sqlSession;
    private MapperManage mapperManage;

    @BeforeEach
    public void init() {
        BeeDataSourceConfig config = new BeeDataSourceConfig();
        config.setDriverClassName("org.sqlite.JDBC");
        config.setJdbcUrl("jdbc:sqlite:d://sqlite-mybatis-magic.db");
        config.setMaxActive(1);
        BeeDataSource ds = new BeeDataSource(config);
        HikariConfig conf = new HikariConfig();
        conf.setJdbcUrl("jdbc:sqlite:d://sqlite-mybatis-magic.db");
        conf.setUsername("postgres");
        conf.setPassword("123456");
        conf.setMaximumPoolSize(2);
        HikariDataSource dataSource = new HikariDataSource(conf);

        MagicConfiguration configuration = new MagicConfiguration(dataSource);
        configuration.addMapperXml("mapper");
        SqlSessionFactory sessionFactory = configuration.build();
        sqlSession = sessionFactory.openSession();
        TestUtils.initScript("script/init-sqlite.sql", sqlSession);
        mapperManage = new MapperManageImpl(sqlSession.getConfiguration(), sqlSession);
    }

    @Test
    public void select() {
        List<UserEntityCore> list = mapperManage.selectAll(UserEntityCore.class);
        System.out.println(list);
    }

    /*@Test
    public void insert() {
        UserEntityCore userEntity = new UserEntityCore();
        userEntity.setId(1L);
        userEntity.setUsername("lingkang");
        userEntity.setPassword("123456");
        userEntity.setNickname("lingkang");
    }*/
}
