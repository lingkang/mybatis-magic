package test.sqlite;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import test.all.AllTest03_Delete;
import top.lingkang.mm.orm.MapperManageImpl;
import top.lingkang.mm.orm.Query;
import top.lingkang.mmt.mapper.UserMapper;
import utils.TestUtils;

import java.util.Arrays;

/**
 * @author lingkang
 * @create by 2024/3/11 14:48
 */
@Slf4j
public class Test03Delete extends AllTest03_Delete {
    @BeforeEach
    public void init() {
        sqlSession = TestUtils.getSqlite();
        TestUtils.initScript("script/init-sqlite.sql", sqlSession);
        mapperManage = new MapperManageImpl(sqlSession.getConfiguration(), sqlSession);
    }

    @Test
    public void testDelete01() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        int count = mapper.deleteByQuery(new Query().eq("id", 1));
        System.out.println(count);
    }

    @Test
    public void delete07() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        int count = mapper.deleteByQuery(new Query().in("id", Arrays.asList(1, 19951219, 3)));
        System.out.println(count);
    }

}
