package test.all;

import org.apache.ibatis.session.SqlSession;
import org.junit.jupiter.api.Test;
import top.lingkang.mm.orm.MapperManage;
import top.lingkang.mm.orm.QueryColumn;
import top.lingkang.mm.page.PageHelper;
import top.lingkang.mm.page.PageInfo;
import top.lingkang.mmt.entity.UserEntity;
import top.lingkang.mmt.mapper.UserMapper;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lingkang
 * Create by 2025/1/20 2:56
 */
public class Test99_Bug_Fix {
    public SqlSession sqlSession;
    public MapperManage mapperManage;

    /**
     * 2025-01-20 修复分页入参类型转换错误
     */
    @Test
    public void queryOne() {
        Map<String, Object> map = new HashMap<>();
        map.put("id", 123);
        int execute = mapperManage.createUpdate("insert into t_user(id) values(#{id});")
                .addParam(map)
                .execute();
        System.out.println(execute);
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        QueryColumn queryColumn = new QueryColumn(UserEntity.class, "id", "username").orderByAsc("id");
        queryColumn.eq("id", 1);
        PageHelper.startPage(1, 10);
        System.out.println(mapper.selectColumn(queryColumn));
        PageInfo pageInfo = PageHelper.getPage();
        System.out.println(pageInfo);
    }

}
