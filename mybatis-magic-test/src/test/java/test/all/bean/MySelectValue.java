package test.all.bean;

import lombok.Data;

/**
 * @author lingkang
 * Created by 2024/3/20
 */
@Data
public class MySelectValue {
    private String id;
    private String nickname;
}
