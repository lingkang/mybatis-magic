package test.all;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.junit.jupiter.api.Test;
import top.lingkang.mmt.mapper.IdEntityAssignMapper;
import top.lingkang.mm.orm.MapperManage;
import top.lingkang.mm.transaction.TransactionManage;
import top.lingkang.mmt.entity.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lingkang
 * @create by 2024/3/11 14:48
 */
@Slf4j
public class AllTest02_Insert {
    public SqlSession sqlSession;
    protected MapperManage mapperManage;

    @Test
    public void insert01() {
        UserEntity entity = new UserEntity();
        entity.setId(System.currentTimeMillis());
        TransactionManage.beginTransaction();
        int insert = mapperManage.insert(entity);
        TransactionManage.commit();
        log.info("影响的行数： {}", insert);
    }

    @Test
    public void insert05() {
        UserEntity entity = new UserEntity();
        entity.setId(System.currentTimeMillis());
        entity.setUsername("lingkang");
        TransactionManage.beginTransaction();
        int insert = mapperManage.insert(entity, false);
        TransactionManage.commit();
        log.info("影响的行数： {} , 结果: {}", insert, entity);
    }

    @Test
    public void insert06() {
        UserEntity entity = new UserEntity();
        entity.setId(System.currentTimeMillis());
        entity.setUsername("lingkang");
        TransactionManage.beginTransaction();
        int insert = mapperManage.insert(entity, false);
        TransactionManage.commit();
        log.info("影响的行数： {} , 结果: {}", insert, entity);
    }

    @Test
    public void insert07() {
        UserEntityExtends entity = new UserEntityExtends();
        entity.setId(System.currentTimeMillis());
        entity.setUsername("lingkang");
        TransactionManage.beginTransaction();
        int insert = mapperManage.insert(entity, false);
        TransactionManage.commit();
        log.info("影响的行数： {} , 结果: {}", insert, entity);
    }

    @Test
    public void insert08() {
        List<UserEntityExtends> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            UserEntityExtends entity = new UserEntityExtends();
            entity.setId(System.currentTimeMillis() + i * 100);
            entity.setUsername("lingkang" + i);
            list.add(entity);
        }
        int insert = mapperManage.insertBatch(list);
        log.info("影响的行数： {} , 结果: {}", insert, list);
    }

    @Test
    public void insert09() {
        List<UserEntityExtends> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            UserEntityExtends entity = new UserEntityExtends();
            entity.setId(System.currentTimeMillis() + i * 100);
            entity.setUsername("lingkang" + i);
            list.add(entity);
        }
        int insert = mapperManage.insertBatch(list);
        log.info("影响的行数： {} , 结果: {}", insert, list);
    }

    @Test
    public void insert10() {
        List<OrderEntity> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            OrderEntity entity = new OrderEntity();
            entity.setId(System.currentTimeMillis() + i + "");
            entity.setStatus(i + "");
            entity.setUserId(i + System.currentTimeMillis());
            list.add(entity);
        }
        int insert = mapperManage.insertBatch(list);
        log.info("影响的行数： {} , 结果: {}", insert, list);
    }

    @Test
    public void insert11() {
        UserNotIdEntity entity = new UserNotIdEntity();
        entity.setId(System.currentTimeMillis() + "");
        entity.setUsername("lingkang");
        entity.setPassword("123");
    }

    @Test
    public void insert12() {
        IdEntityAssign assign = new IdEntityAssign();
        assign.setName("lingkang");
        mapperManage.insert(assign);
        System.out.println(assign);
    }

    @Test
    public void insert13() {
        IdEntityAssign assign = new IdEntityAssign();
        assign.setName("lingkang");
        IdEntityAssignMapper mapper = mapperManage.getMapper(IdEntityAssignMapper.class);
        mapper.insert(assign);
        System.out.println(assign);
    }

}
