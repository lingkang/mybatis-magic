package test.all;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.junit.jupiter.api.Test;
import top.lingkang.mm.orm.MapperManage;
import top.lingkang.mm.orm.Query;
import top.lingkang.mmt.entity.UserEntity;
import top.lingkang.mmt.entity.UserEntityExtends;
import top.lingkang.mmt.mapper.UserEntityExtendsMapper;
import top.lingkang.mmt.mapper.UserMapper;

import java.util.*;

/**
 * @author lingkang
 * @create by 2024/3/11 15:03
 */
@Slf4j
public class AllTest03_Delete {

    public SqlSession sqlSession;
    protected MapperManage mapperManage;

    @Test
    public void delete01() {
        List<Long> ids = new ArrayList<>();
        ids.add(1L);
        int delete = mapperManage.deleteByIds(UserEntity.class, ids);
        log.info("影响的行数： {} ", delete);
    }

    @Test
    public void delete02() {
        int delete = mapperManage.deleteById(UserEntity.class, 19951219L);
        log.info("影响的行数： {} ", delete);
    }

    @Test
    public void delete03() {
        UserEntity entity = new UserEntity();
        entity.setId(1761052961150074880L);
        mapperManage.deleteById(entity);
    }

    @Test
    public void delete04() {
        UserEntityExtends entity = new UserEntityExtends();
        entity.setId(1L);
        mapperManage.deleteById(entity);
    }


    @Test
    public void delete05() {
        sqlSession.getConfiguration().addMapper(UserEntityExtendsMapper.class);
        UserEntityExtendsMapper mapper = mapperManage.getMapper(UserEntityExtendsMapper.class);
        UserEntityExtends entity = new UserEntityExtends();
        entity.setId(1L);
        mapper.deleteById(entity);
    }

    @Test
    public void delete06() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        int count = mapper.deleteByQuery(new Query().eq("id", 1));
        System.out.println(count);
    }

    @Test
    public void delete07() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        int count = mapper.deleteByQuery(new Query().in("id", Arrays.asList(1, 19951219, 3)));
        System.out.println(count);
    }
}
