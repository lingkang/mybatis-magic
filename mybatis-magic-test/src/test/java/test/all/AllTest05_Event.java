package test.all;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.junit.jupiter.api.Test;
import top.lingkang.mm.orm.MapperManage;
import top.lingkang.mmt.entity.UserEntityEvent;

import java.util.List;

/**
 * @author lingkang
 * @create by 2024/3/11 15:18
 */
@Slf4j
public class AllTest05_Event {
    public SqlSession sqlSession;
    public MapperManage mapperManage;

    @Test
    public void update() {
        List<UserEntityEvent> list = mapperManage.selectAll(UserEntityEvent.class);
        UserEntityEvent user = list.get(0);
        log.info("原: " + user);
        user.setUsername("updateById");
        user.setPassword(System.currentTimeMillis() + "");
        int update = mapperManage.updateById(user);
        log.info("影响的行数： {}", update);
        log.info("新: " + user);
    }

}
