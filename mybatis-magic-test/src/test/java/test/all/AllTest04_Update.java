package test.all;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.junit.jupiter.api.Test;
import top.lingkang.mm.orm.MapperManage;
import top.lingkang.mmt.entity.UserEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lingkang
 * @create by 2024/3/11 15:03
 */
@Slf4j
public class AllTest04_Update {

    public SqlSession sqlSession;
    protected MapperManage mapperManage;

    @Test
    public void update() {
        List<UserEntity> list = mapperManage.selectAll(UserEntity.class);
        UserEntity user = list.get(0);
        log.info("原: " + user);
        user.setUsername("updateById");
        user.setPassword(System.currentTimeMillis() + "");
        int update = mapperManage.updateById(user);
        log.info("影响的行数： {}", update);
        log.info("新: " + user);
    }

    @Test
    public void update2() {
        List<UserEntity> list = mapperManage.selectAll(UserEntity.class);
        UserEntity user = list.get(0);
        log.info("原: " + user);
        user.setUsername("updateById-false");
        user.setPassword(System.currentTimeMillis() + "");
        int update = mapperManage.updateById(user, false);
        log.info("影响的行数： {}", update);
        log.info("新: " + user);
    }
}
