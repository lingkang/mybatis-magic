package test.all;

import cn.hutool.core.io.IoUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.junit.jupiter.api.Test;
import top.lingkang.mm.orm.MapperManage;
import top.lingkang.mm.page.PageHelper;
import top.lingkang.mm.page.PageInfo;
import top.lingkang.mmt.entity.UserEntity;
import top.lingkang.mmt.entity.UserEntityExtends;
import top.lingkang.mmt.mapper.UserMapper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lingkang
 * @create by 2024/3/11 14:09
 */
@Slf4j
public class AllTest01_Select {
    protected SqlSession sqlSession;
    protected MapperManage mapperManage;

    @Test
    public void test01() throws Exception {
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        System.out.println(mapper.selectAll());
        mapper.selectAll();
        mapper.selectAll();
        mapper.selectAll();
        mapper.selectAll();
    }

    @Test
    public void test02() throws Exception {
        Connection connection = sqlSession.getConnection();
        ResultSet resultSet = connection.prepareStatement("select * from t_user").executeQuery();
        while (resultSet.next())
            System.out.println(resultSet.getObject(1));
        IoUtil.close(connection);
        connection = sqlSession.getConnection();
        IoUtil.close(connection);
        connection = sqlSession.getConnection();
        IoUtil.close(connection);
        connection = sqlSession.getConnection();
        IoUtil.close(connection);
        connection = sqlSession.getConnection();
        IoUtil.close(connection);
    }

    @Test
    public void test04() {
        List<UserEntity> list = mapperManage.selectAll(UserEntity.class);
        log.info("selectAll " + list);
    }

    @Test
    public void test05() {
        UserEntity userEntity = mapperManage.selectById(UserEntity.class, 1);
        System.out.println(userEntity);
    }

    @Test
    public void test06() {
        UserEntity userEntity = sqlSession.selectOne("top.lingkang.mmt.mapper.UserMapper.selectById", 1);
        System.out.println(userEntity);
    }

    @Test
    public void test07() {
        log.info("分页查询无入参");
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        // 开始分页
        PageHelper.startPage(1, 4);
        // 查询
        List<UserEntity> list = mapper.selectAll();
        System.out.println(list);
        // 获取到分页的内容
        PageInfo page = PageHelper.getPage();

        System.out.println(page);
        System.out.println(page.countPageNumber());
    }

    @Test
    public void test07_2() {
        log.info("分页查询无入参");
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        // 开始分页
        PageHelper.startPage(2, 4);
        // 查询
        List<UserEntity> list = mapper.selectAll();
        System.out.println(list);
        // 获取到分页的内容
        PageInfo page = PageHelper.getPage();

        System.out.println(page);
        System.out.println(page.countPageNumber());
    }

    @Test
    public void test08() {
        log.info("分页查询Map入参");
        PageHelper.startPage(1, 3);
        Map<String, Long> map = new HashMap<>();
        map.put("id1", 1L);
        map.put("id2", 2L);
        map.put("id3", 19951219L);
        List<UserEntity> list = sqlSession.selectList("selectIn", map);
        log.info("分页查询: {}", list);
        PageInfo page = PageHelper.getPage();
        System.out.println(page);
    }

    @Test
    public void test09() {
        log.info("分页查询对象入参");
        PageHelper.startPage(1, 3);
        UserEntity user = new UserEntity();
        user.setId(1L);
        user.setUsername(null);
        List<UserEntity> list = sqlSession.selectList("selectObj", user);
        log.info("分页查询: {}", list);
        PageInfo page = PageHelper.getPage();
        System.out.println(page);
    }

    @Test
    public void test10() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        List<UserEntity> list = mapper.selectAll();
        System.out.println(list);
        List<UserEntity> all = mapper.all();
        System.out.println(all);
    }

    @Test
    public void test11() {
        List<UserEntityExtends> list = mapperManage.selectAll(UserEntityExtends.class);
        System.out.println(list);
    }

    @Test
    public void test12() {
        boolean exists = mapperManage.existsById(UserEntity.class, 1);
        log.info("existsById: {}", exists);
    }
}
