package test.all;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.junit.jupiter.api.Test;
import top.lingkang.mm.orm.MapperManage;
import top.lingkang.mm.orm.QueryColumn;
import top.lingkang.mm.orm.QueryWrapper;
import top.lingkang.mm.page.PageHelper;
import top.lingkang.mm.page.PageInfo;
import top.lingkang.mmt.entity.UserEntity;
import top.lingkang.mmt.mapper.UserMapper;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lingkang
 * @create by 2024/3/11 15:18
 */
@Slf4j
public class AllTest11_CreateQuery {
    public SqlSession sqlSession;
    public MapperManage mapperManage;

    @Test
    public void query() {
        QueryWrapper<UserEntity> query = mapperManage.createQuery(
                "select * from t_user where id>#{id}",
                UserEntity.class);
        query.addParam("id", 2);
        List<UserEntity> list = query.getList();
        System.out.println(list);
    }

    @Test
    public void query2() {
        QueryWrapper<Date> query = mapperManage.createQuery(
                "select create_time from t_user where id>#{id}",
                Date.class);
        query.addParam("id", 2);
        List<Date> list = query.getList();
        System.out.println(list);
        // System.out.println(list.get(list.size() - 1).getClass());
    }

    @Test
    public void query3() {
        log.info("开启分页");
        PageHelper.startPage(1, 5);
        QueryWrapper<Long> query = mapperManage.createQuery(
                "select id from t_user where id>#{id}",
                Long.class);
        query.addParam("id", 2);
        List<Long> list = query.getList();
        System.out.println(list);
        System.out.println(list.get(0).getClass());
        PageInfo page = PageHelper.getPage();
        log.info("分页结果: {}", page);
    }

    @Test
    public void queryOne() {
        QueryWrapper<Long> query = mapperManage.createQuery(
                "select id from t_user where id=#{id}",
                Long.class);
        query.addParam("id", 1);
        Long one = query.getOne();
        System.out.println(one);
        System.out.println(one.getClass());

        query.addParam("id", 2);
        one = query.getOne();
        System.out.println(one);
    }

    @Test
    public void createUpdate() {
        Map<String, Object> map = new HashMap<>();
        map.put("id", 123);
        int execute = mapperManage.createUpdate("insert into t_user(id) values(#{id});")
                .addParam(map)
                .execute();
        System.out.println(execute);
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        System.out.println(mapper.selectColumn(new QueryColumn(String.class, "id")));
    }

    @Test
    public void queryOne2() {
        QueryWrapper<Map> query = mapperManage.createQuery(
                "select * from t_user where id=#{id}",
                Map.class);
        query.addParam("id", 1);
        Map one = query.getOne();
        System.out.println(one);
        System.out.println(one.getClass());

        query.addParam("id", 2);
        one = query.getOne();
        System.out.println(one);
    }

    @Test
    public void queryOne3() {
        QueryWrapper<UserEntity> query = mapperManage.createQuery(
                "select * from t_user where id=#{id}",
                UserEntity.class);
        query.addParam("id", 1);
        UserEntity one = query.getOne();
        System.out.println(one);
        System.out.println(one.getClass());

        query.addParam("id", 2);
        one = query.getOne();
        System.out.println(one);
    }

    @Test
    public void query11() {
        PageHelper.startPage(1, 5);
        QueryWrapper<UserEntity> query = mapperManage.createQuery(
                "  select * from t_user",
                UserEntity.class);
        System.out.println(query.getList());
        System.out.println(PageHelper.getPage());
    }

}
