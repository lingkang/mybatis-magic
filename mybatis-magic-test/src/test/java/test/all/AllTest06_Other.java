package test.all;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.junit.jupiter.api.Test;
import top.lingkang.mm.orm.MapperManage;
import top.lingkang.mmt.entity.UserEntity;
import top.lingkang.mmt.entity.UserEntityEvent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author lingkang
 * @create by 2024/3/11 15:18
 */
@Slf4j
public class AllTest06_Other {
    public SqlSession sqlSession;
    public MapperManage mapperManage;

    @Test
    public void test01() {
        List<UserEntity> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            UserEntity entity = new UserEntity();
            entity.setId(System.currentTimeMillis() + i*1000);
            entity.setUsername("username" + i);
            entity.setPassword("password" + i);
            entity.setCreateTime(new Date());
            entity.setUpdateTime(entity.getCreateTime());
            list.add(entity);
        }
        int insert = sqlSession.insert("other.insertList", list);
        log.info("影响的行数： {}", insert);
    }

    @Test
    public void test02() {
        String s = mapperManage.selectTableSql(UserEntity.class);
        log.info("selectColumn: {}", s);
    }

}
