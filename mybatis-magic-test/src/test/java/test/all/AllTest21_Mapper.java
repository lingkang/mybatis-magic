package test.all;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.junit.jupiter.api.Test;
import test.all.bean.MySelectValue;
import test.mapper.NotIdUserEntity;
import test.mapper.NotIdUserMapper;
import top.lingkang.mm.MagicConfiguration;
import top.lingkang.mm.orm.MapperManage;
import top.lingkang.mm.orm.Query;
import top.lingkang.mm.orm.QueryColumn;
import top.lingkang.mm.orm.UpdateColumn;
import top.lingkang.mm.page.PageHelper;
import top.lingkang.mm.page.PageInfo;
import top.lingkang.mm.utils.MapParam;
import top.lingkang.mmt.entity.UserEntity;
import top.lingkang.mmt.mapper.OrderMapper;
import top.lingkang.mmt.mapper.UserAutoTimeMapper;
import top.lingkang.mmt.mapper.UserMapper;

import java.util.*;

/**
 * @author lingkang
 * Created by 2024/3/14
 */
@Slf4j
public class AllTest21_Mapper {
    public SqlSession sqlSession;
    public MapperManage mapperManage;


    @Test
    public void test01() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        log.info("queryAll: {}", mapper.selectAll());
    }

    @Test
    public void test02() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        UserEntity entity = mapper.selectById(1);
        log.info("queryById: {}", entity);
    }

    @Test
    public void test03() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        boolean entity = mapper.existsById(1);
        log.info("existsById: {}", entity);
    }

    @Test
    public void test04() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        List<UserEntity> list = mapper.createQuery(new Query().eq("id", 1));
        log.info("createQuery: {}", list);
    }

    @Test
    public void test05() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        UserEntity entity = new UserEntity();
        entity.setId(System.currentTimeMillis());
        mapper.insert(entity);
        log.info("save: {}", entity);
        System.out.println(entity.getCreateTime());
    }

    @Test
    public void test06() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        PageHelper.startPage(1, 5);
        List<UserEntity> list = mapper.selectAll();
        PageInfo page = PageHelper.getPage();
        log.info("page: {} {}", page, list);
    }

    @Test
    public void test08() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        UserEntity entity = new UserEntity();
        entity.setId(1L);
        entity.setUsername("lk");
        int updateById = mapper.updateById(entity);
        log.info("受影响的行数 {}", updateById);
    }

    @Test
    public void test09() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        UserEntity entity = mapper.selectById(1L);
        log.info("selectById {}", entity);
    }

    @Test
    public void test49() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        UserEntity entity = mapper.selectById(null);
        log.info("selectById {}", entity);
    }

    // @Test
    public void test10() {
        MagicConfiguration configuration = (MagicConfiguration) sqlSession.getConfiguration();
        configuration.addMapper(NotIdUserMapper.class);

        NotIdUserMapper mapper = mapperManage.getMapper(NotIdUserMapper.class);
        NotIdUserEntity entity = mapper.selectById(1L);
        log.info("selectById {}", entity);
    }

    @Test
    public void test11() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        boolean b = mapper.existsById(1L);
        log.info("existsById {}", b);
    }

    @Test
    public void test12() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        boolean b = mapper.existsById(66L);
        log.info("existsById {}", b);
    }

    @Test
    public void test13() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        int b = mapper.deleteById(1L);
        log.info("deleteById {}", b);
    }

    @Test
    public void test14() {
        List<UserEntity> list = new ArrayList<>();
        UserEntity user = new UserEntity();
        user.setId(System.currentTimeMillis());
        UserEntity user1 = new UserEntity();
        user1.setId(System.currentTimeMillis() + 100L);
        list.add(user);
        list.add(user1);
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        int insertBatch = mapper.insertBatch(list);
        log.info("insertBatch list: {}", list);
        log.info("insertBatch {}", insertBatch);
    }

    @Test
    public void selectColumn() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        List<MySelectValue> list = mapper.selectColumn(
                new QueryColumn(MySelectValue.class, "id", "nickname").gt("id", 2)
        );
        log.info("selectColumn: {}", list);
    }

    @Test
    public void selectColumn2() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        List<Long> ids = new ArrayList<>();
        ids.add(1L);
        ids.add(19951219L);
        ids.add(1761068137131802624L);
        List<MySelectValue> list = mapper.selectColumn(
                new QueryColumn(MySelectValue.class, "id", "nickname").in("id", ids)
        );
        log.info("selectColumn2: {}", list);
    }

    @Test
    public void selectColumn3() {
        UserMapper userMapper = mapperManage.getMapper(UserMapper.class);
        List<Long> list = userMapper.selectColumn(
                new QueryColumn(Long.class, "id")
                        .gt("id", 1) // 大于1 的id列
        );
        log.info("selectColumn3: {}", list);
    }

    @Test
    public void selectColumn4() {
        UserMapper userMapper = mapperManage.getMapper(UserMapper.class);
        List<Long> list = userMapper.selectColumn(
                new QueryColumn(Long.class, "id"));
        log.info("selectColumn4: {}", list);
    }

    @Test
    public void selectColumnOne() {
        UserMapper userMapper = mapperManage.getMapper(UserMapper.class);
        MySelectValue result = userMapper.selectColumnOne(
                new QueryColumn(MySelectValue.class, "id", "username").sql(" limit 1"));
        log.info("selectColumnOne: {}", result);
    }

    @Test
    public void selectColumnOne2() {
        UserMapper userMapper = mapperManage.getMapper(UserMapper.class);
        Long result = userMapper.selectColumnOne(
                new QueryColumn(Long.class, "id").sql("limit 1"));
        log.info("selectColumnOne2: {}", result);
    }

    @Test
    public void updateByQuery() {
        UserMapper userMapper = mapperManage.getMapper(UserMapper.class);
        List<UserEntity> list = userMapper.selectAll();
        log.info("list: {}", list);
        UserEntity user = list.get(0);
        user.setUsername("lk");
        user.setPassword("updateByQuery");
        user.setNickname("update");
        int count = userMapper.updateByQuery(user, new Query().eq("id", user.getId()));
        log.info("count: {}", count);
        list = userMapper.selectAll();
        log.info("list: {}", list);
    }

    @Test
    public void count() {
        UserMapper userMapper = mapperManage.getMapper(UserMapper.class);
        log.info("count: {}", userMapper.selectCount());
    }

    @Test
    public void createQuery() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        List<UserEntity> list = mapper.createQuery(new Query().in("id", Arrays.asList(1, 19951219, 3)));
        log.info("createQuery: {}", list);
    }

    @Test
    public void existsByQuery() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        boolean has = mapper.existsByQuery(new Query().eq("id", 1));
        log.info("existsByQuery: {}", has);
    }

    @Test
    public void selectCountByQuery() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        long total = mapper.selectCountByQuery(new Query().gt("id", 1)); // 大于 >
        log.info("total: {}", total);
        total = mapper.selectCountByQuery(new Query().le("id", 1));  // 小于等于 <=
        log.info("total: {}", total);
    }

    @Test
    public void selectByQuery() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        List<UserEntity> list = mapper.selectByQuery(new Query().gt("id", 1)); // 大于 >
        log.info("selectByQuery: {}", list);
        list = mapper.selectByQuery(new Query().le("id", 1));  // 小于等于 <=
        log.info("selectByQuery: {}", list);
    }

    @Test
    public void selectByQueryOne() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        UserEntity entity = mapper.selectByQueryOne(new Query().eq("id", 1));
        log.info("selectByQueryOne: {}", entity);
        entity = mapper.selectByQueryOne(new Query().gt("id", 1).sql("limit 1"));  //  大于 >
        log.info("selectByQueryOne: {}", entity);
    }

    @Test
    public void selectColumn22() {
        UserMapper userMapper = mapperManage.getMapper(UserMapper.class);
        List<HashMap<String, Object>> result = userMapper.selectColumn(
                new QueryColumn(HashMap.class).sql("limit 10"));
        log.info("selectColumn: {}", result);
    }

    @Test
    public void selectByQueryPage() {
        UserMapper userMapper = mapperManage.getMapper(UserMapper.class);
        Query query = new Query().orderByDesc("create_time");
        query.like("id", 199);
        PageHelper.startPage(1, 10);
        Object o = userMapper.selectByQuery(query);
        PageInfo page = PageHelper.getPage();
        System.out.println(o);
        System.out.println(page);
    }

    @Test
    public void updateByColumn() {
        UserAutoTimeMapper userMapper = mapperManage.getMapper(UserAutoTimeMapper.class);
        int i = userMapper.updateByColumn(new UpdateColumn().set("id", 66).eq("id", 1));
        System.out.println("受影响的行：" + i);
        OrderMapper mapper = mapperManage.getMapper(OrderMapper.class);
        System.out.println(mapper.selectAll());
    }

    @Test
    public void updateByColumn2() {
        UserMapper userMapper = mapperManage.getMapper(UserMapper.class);
        int i = userMapper.updateByColumn(new UpdateColumn().set("id", 66).eq("id", 1)
                .notIn("password", Arrays.asList("lk", "1995")));
        System.out.println("受影响的行：" + i);
        OrderMapper mapper = mapperManage.getMapper(OrderMapper.class);
        System.out.println(mapper.selectAll());
    }

    @Test
    public void existsByEntity() {
        UserMapper userMapper = mapperManage.getMapper(UserMapper.class);
        UserEntity entity = new UserEntity();
        entity.setId(1L);
        boolean exists = userMapper.existsByEntity(entity);
        System.out.println("存在：" + exists);
    }

    @Test
    public void insertOrUpdate() {
        UserMapper userMapper = mapperManage.getMapper(UserMapper.class);
        UserEntity entity = new UserEntity();
        entity.setId(1234566L);
        int insert = userMapper.insertOrUpdate(entity);
        System.out.println("受影响的行：" + insert);
        System.out.println(userMapper.selectAll());
    }

    @Test
    public void selectInIds() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        List<UserEntity> selectInIds = mapper.selectInIds(Arrays.asList("1", "19951219"));
        log.info("selectInIds: {}", selectInIds);
    }

    @Test
    public void deleteInIds() {
        UserMapper userMapper = mapperManage.getMapper(UserMapper.class);
        int deleteInIds = userMapper.deleteInIds(Arrays.asList("1", "19951219"));
        log.info("deleteInIds: {}", deleteInIds);
    }

    @Test
    public void selectToMap() {
        List<Map> list = mapperManage.selectToMap("select * from t_user");
        log.info("selectToMap: {}", list);
    }

    @Test
    public void selectToMap2() {
        List<Map> list = mapperManage.selectToMap(
                "select * from t_user where id=#{id}",
                MapParam.create("id", 1)
        );
        log.info("selectToMap: {}", list);
    }

    @Test
    public void selectToMapOne() {
        Map map = mapperManage.selectToMapOne(
                "select * from t_user where id=1");
        log.info("selectToMapOne: {}", map);
    }

    @Test
    public void selectToMapOne2() {
        Map map = mapperManage.selectToMapOne(
                "select * from t_user where id=#{id}",
                MapParam.create("id", 1)
        );
        log.info("selectToMapOne: {}", map);
    }

    @Test
    public void updateSql() {
        int result = mapperManage.updateSql("update t_user set nickname='lk' where id=1");
        log.info("updateSql: {}", result);
    }

    @Test
    public void updateSql2() {
        int result = mapperManage.updateSql(
                "update t_user set nickname='lk' where id=#{id}",
                MapParam.create("id", 1)
        );
        log.info("updateSql: {}", result);
    }

    @Test
    public void symbol() {
        UserMapper userMapper = mapperManage.getMapper(UserMapper.class);
        UserEntity entity = userMapper.selectByQueryOne(
                new Query().symbol("id", "=", 1)
        );
        log.info("selectByQueryOne: {}", entity);
        int result = userMapper.updateByColumn(new UpdateColumn().set("id", 2).symbol("id", "=", 1));
        log.info("updateByColumn: {}", result);
        String id = userMapper.selectColumnOne(
                new QueryColumn(String.class, "id")
                        .symbol("id", "=", 2)
        );
        log.info("selectColumnOne: {}", id);
    }

    @Test
    public void isNull() {
        UserMapper userMapper = mapperManage.getMapper(UserMapper.class);
        List<UserEntity> list = userMapper.selectByQuery(new Query().isNull("nickname"));
        log.info("list: {}", list);
    }

    @Test
    public void isNotNull() {
        UserMapper userMapper = mapperManage.getMapper(UserMapper.class);
        List<UserEntity> list = userMapper.selectByQuery(new Query().isNotNull("nickname"));
        log.info("list: {}", list);
    }

    @Test
    public void notLike() {
        UserMapper userMapper = mapperManage.getMapper(UserMapper.class);
        List<UserEntity> list = userMapper.selectByQuery(new Query().notLike("username", "lk"));
        log.info("list: {}", list);
    }

    @Test
    public void likeLeft() {
        UserMapper userMapper = mapperManage.getMapper(UserMapper.class);
        List<UserEntity> list = userMapper.selectByQuery(new Query().likeLeft("username", "19374"));
        log.info("list: {}", list);
    }

    @Test
    public void likeRight() {
        UserMapper userMapper = mapperManage.getMapper(UserMapper.class);
        List<UserEntity> list = userMapper.selectByQuery(new Query().likeRight("username", "17087027992"));
        log.info("list: {}", list);
    }

    @Test
    public void notLikeLeft() {
        UserMapper userMapper = mapperManage.getMapper(UserMapper.class);
        List<UserEntity> list = userMapper.selectByQuery(new Query().notLikeLeft("username", "19374"));
        log.info("list: {}", list);
    }

    @Test
    public void notLikeRight() {
        UserMapper userMapper = mapperManage.getMapper(UserMapper.class);
        List<UserEntity> list = userMapper.selectByQuery(new Query().notLikeRight("username", "17087027992"));
        log.info("list: {}", list);
    }
}
