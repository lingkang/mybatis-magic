package test.all;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.junit.jupiter.api.Test;
import test.mapper.NotIdUserEntity;
import test.mapper.NotIdUserMapper;
import top.lingkang.mm.MagicConfiguration;
import top.lingkang.mm.orm.MapperManage;
import top.lingkang.mm.orm.Query;
import top.lingkang.mm.orm.UpdateWrapper;
import top.lingkang.mm.page.PageHelper;
import top.lingkang.mm.page.PageInfo;
import top.lingkang.mmt.entity.UserEntity;
import top.lingkang.mmt.mapper.UserMapper;

import java.util.List;

/**
 * @author lingkang
 * @create by 2024/3/11 15:18
 */
@Slf4j
public class AllTest12_CreateUpdate {
    public SqlSession sqlSession;
    public MapperManage mapperManage;

    @Test
    public void update() {
        PageHelper.startPage(1,10);
        UpdateWrapper update = mapperManage.createUpdate("update t_user set password=#{p} where id=#{id}");
        update.addParam("p", System.currentTimeMillis()).addParam("id", 1);
        int execute = update.execute();
        log.info("受影响行数: {}", execute);
        PageInfo page = PageHelper.getPage();
        System.out.println(page);
    }

    @Test
    public void update1() {
        UpdateWrapper update = mapperManage.createUpdate("insert into t_user(id,username) values (2,#{un})");
        update.addParam("un", "lk");
        int execute = update.execute();
        log.info("受影响行数: {}", execute);
    }

    @Test
    public void test01() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        log.info("queryAll: {}", mapper.selectAll());
    }

    @Test
    public void test02() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        UserEntity entity = mapper.selectById(1);
        log.info("queryById: {}", entity);
    }

    @Test
    public void test03() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        boolean entity = mapper.existsById(1);
        log.info("existsById: {}", entity);
    }

    @Test
    public void test04() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        List<UserEntity> list = mapper.createQuery(new Query().eq("id", 1));
        log.info("createQuery: {}", list);
    }

    @Test
    public void test05() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        UserEntity entity = new UserEntity();
        entity.setId(System.currentTimeMillis());
        mapper.insert(entity);
        log.info("save: {}", entity);
        System.out.println(entity.getCreateTime());
    }

    @Test
    public void test06() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        PageHelper.startPage(1, 5);
        List<UserEntity> list = mapper.selectAll();
        PageInfo page = PageHelper.getPage();
        log.info("page: {} {}", page, list);
    }

    @Test
    public void test08() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        UserEntity entity = new UserEntity();
        entity.setId(1L);
        entity.setUsername("lk");
        int updateById = mapper.updateById(entity);
        log.info("受影响的行数 {}", updateById);
    }

    @Test
    public void test09() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        UserEntity entity = mapper.selectById(1L);
        log.info("selectById {}", entity);
    }

    @Test
    public void test49() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        UserEntity entity = mapper.selectById(null);
        log.info("selectById {}", entity);
    }

    // @Test
    public void test10() {
        MagicConfiguration configuration = (MagicConfiguration) sqlSession.getConfiguration();
        configuration.addMapper(NotIdUserMapper.class);

        NotIdUserMapper mapper = mapperManage.getMapper(NotIdUserMapper.class);
        NotIdUserEntity entity = mapper.selectById(1L);
        log.info("selectById {}", entity);
    }

    @Test
    public void test11() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        boolean b = mapper.existsById(1L);
        log.info("existsById {}", b);
    }

    @Test
    public void test12() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        boolean b = mapper.existsById(66L);
        log.info("existsById {}", b);
    }

    @Test
    public void test13() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        int b = mapper.deleteById(1L);
        log.info("deleteById {}", b);
    }

}
