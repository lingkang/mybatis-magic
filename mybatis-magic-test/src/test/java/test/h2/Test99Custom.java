package test.h2;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import top.lingkang.mm.orm.MapperManage;
import top.lingkang.mm.orm.MapperManageImpl;
import top.lingkang.mmt.entity.UserNotIdEntity;
import utils.TestUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lingkang
 * Create by 2024/9/11 0:43
 */
@Slf4j
public class Test99Custom {
    public SqlSession sqlSession;
    public MapperManage mapperManage;

    @BeforeEach
    public void init() {
        sqlSession = TestUtils.getH2();
        TestUtils.initScript("script/init-h2.sql", sqlSession);
        mapperManage = new MapperManageImpl(sqlSession.getConfiguration(), sqlSession);
    }

    @Test
    public void test01() {
        UserNotIdEntity entity = new UserNotIdEntity();
        entity.setId(System.currentTimeMillis() + "");
        entity.setUsername("lingkang");
        entity.setPassword("lingkang");
        mapperManage.insert(entity, false);
    }

    @Test
    public void test02() {
        UserNotIdEntity entity = new UserNotIdEntity();
        entity.setId(System.currentTimeMillis() + "");
        entity.setUsername("lingkang");
        entity.setPassword("lingkang");
        List<UserNotIdEntity> list = new ArrayList<>();
        list.add(entity);
        mapperManage.insertBatch(list);
    }
}
