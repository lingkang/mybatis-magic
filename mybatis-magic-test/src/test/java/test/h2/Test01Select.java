package test.h2;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import test.all.AllTest01_Select;
import top.lingkang.mm.orm.MapperManageImpl;
import utils.TestUtils;

/**
 * @author lingkang
 * @create by 2024/3/11 13:54
 */
@Slf4j
public class Test01Select extends AllTest01_Select {

    @BeforeEach
    public void init() {
        sqlSession = TestUtils.getH2();
        TestUtils.initScript("script/init-h2.sql", sqlSession);
        mapperManage = new MapperManageImpl(sqlSession.getConfiguration(), sqlSession);
    }

}
