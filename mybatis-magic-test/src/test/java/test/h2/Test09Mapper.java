package test.h2;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import test.all.AllTest21_Mapper;
import top.lingkang.mm.orm.MapperManageImpl;
import utils.TestUtils;

/**
 * @author lingkang
 * Created by 2024/3/14
 */
@Slf4j
public class Test09Mapper extends AllTest21_Mapper {
    @BeforeEach
    public void init() {
        sqlSession = TestUtils.getH2();
        TestUtils.initScript("script/init-h2.sql", sqlSession);
        mapperManage = new MapperManageImpl(sqlSession.getConfiguration(), sqlSession);
    }
}
