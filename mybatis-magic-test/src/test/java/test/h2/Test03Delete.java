package test.h2;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import test.all.AllTest03_Delete;
import utils.TestUtils;
import top.lingkang.mm.orm.MapperManageImpl;

/**
 * @author lingkang
 * @create by 2024/3/11 14:48
 */
@Slf4j
public class Test03Delete extends AllTest03_Delete {
    @BeforeEach
    public void init() {
        sqlSession = TestUtils.getH2();
        TestUtils.initScript("script/init-h2.sql", sqlSession);
        mapperManage = new MapperManageImpl(sqlSession.getConfiguration(), sqlSession);
    }

}
