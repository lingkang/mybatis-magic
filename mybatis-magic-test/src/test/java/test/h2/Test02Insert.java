package test.h2;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import test.all.AllTest02_Insert;
import top.lingkang.mmt.entity.IdEntityAssign;
import utils.TestUtils;
import top.lingkang.mm.orm.MapperManageImpl;
import top.lingkang.mm.transaction.TransactionManage;
import top.lingkang.mmt.entity.IdEntity;

/**
 * @author lingkang
 * @create by 2024/3/11 14:48
 */
@Slf4j
public class Test02Insert extends AllTest02_Insert {
    @BeforeEach
    public void init() {
        sqlSession = TestUtils.getH2();
        TestUtils.initScript("script/init-h2.sql", sqlSession);
        mapperManage = new MapperManageImpl(sqlSession.getConfiguration(), sqlSession);
    }

    @Test
    public void insert55() {
        IdEntity entity = new IdEntity();
        entity.setId(95L+"");
        entity.setName("test");
        TransactionManage.beginTransaction();
        int insert = mapperManage.insert(entity);
        TransactionManage.commit();
        log.info("影响的行数： {} , 结果: {}", insert, entity);
    }

    @Test
    public void insert12() {
        IdEntityAssign assign = new IdEntityAssign();
        assign.setName("lingkang");
        mapperManage.insert(assign);
        System.out.println(assign);
    }

}
