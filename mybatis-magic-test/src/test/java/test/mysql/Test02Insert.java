package test.mysql;

import cn.hutool.core.lang.Assert;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import test.all.AllTest02_Insert;
import utils.TestUtils;
import top.lingkang.mm.orm.MapperManageImpl;
import top.lingkang.mm.transaction.TransactionManage;
import top.lingkang.mmt.entity.IdEntity;
import top.lingkang.mmt.entity.IdEntityAssign;

/**
 * @author lingkang
 * @create by 2024/3/1 16:56
 */
@Slf4j
public class Test02Insert extends AllTest02_Insert {
    @BeforeEach
    public void init() {
        sqlSession = TestUtils.getMysql();
        TestUtils.initScript("script/init-mysql.sql", sqlSession);
        mapperManage = new MapperManageImpl(sqlSession.getConfiguration(), sqlSession);
    }


    @Test
    public void insert02() {
        IdEntity entity = new IdEntity();
        entity.setId(System.currentTimeMillis()+"");
        TransactionManage.beginTransaction();
        int insert = mapperManage.insert(entity);
        TransactionManage.commit();
        log.info("影响的行数： {} , 结果: {}", insert, entity);
    }

    @Test
    public void insert03() {
        IdEntityAssign entity = new IdEntityAssign();
        // entity.setId(System.currentTimeMillis());
        TransactionManage.beginTransaction();
        int insert = mapperManage.insert(entity);
        TransactionManage.commit();
        log.info("IdEntityAssign 影响的行数： {} , 结果: {}", insert, entity);

        entity = new IdEntityAssign();
        entity.setId(System.currentTimeMillis()+"");
        TransactionManage.beginTransaction();
        insert = mapperManage.insert(entity);
        TransactionManage.commit();
        log.info("IdEntityAssign 影响的行数： {} , 结果: {}", insert, entity);
    }

    @Test
    public void insert04() {
        IdEntity entity = new IdEntity();
        // entity.setId(System.currentTimeMillis());
        TransactionManage.beginTransaction();
        int insert = 0;
        try {
            insert = mapperManage.insert(entity, false);
            TransactionManage.commit();
        } catch (Exception e) {
            TransactionManage.rollback();
            log.warn("插入检查", e);
        }
        log.info("影响的行数： {} , 结果: {}", insert, entity);
        Assert.isTrue(insert == 0, "插入全空值测试失败");
    }

}
