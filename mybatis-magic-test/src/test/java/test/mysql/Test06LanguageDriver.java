package test.mysql;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.jupiter.api.Test;
import top.lingkang.mm.MagicConfiguration;
import top.lingkang.mmt.entity.UserEntity;
import top.lingkang.mmt.mapper.LanguageDriverMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lingkang
 * @create by 2024/3/6 9:59
 */
public class Test06LanguageDriver {
    @Test
    public void test01() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:mysql://localhost:3306/test?useSSL=true");
        config.setUsername("root");
        config.setPassword("123456");
        config.setMaximumPoolSize(1);
        HikariDataSource dataSource = new HikariDataSource(config);

        MagicConfiguration configuration = new MagicConfiguration(dataSource);
        configuration.setMapUnderscoreToCamelCase(true);
        configuration.addMapper(LanguageDriverMapper.class);
        SqlSessionFactory sessionFactory = configuration.build();
        SqlSession sqlSession = sessionFactory.openSession();
        // MapperManage mapperManage = new MapperManageImpl(configuration, sqlSession);

        Map<String, Object> map = new HashMap<>();
        map.put("id",1);
        List<UserEntity> list = sqlSession.selectList("selectAll", map);
        System.out.println(list);
//        if (!list.isEmpty())
//            System.out.println(list.get(0).getClass());

    }
}
