package test.mysql;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import test.all.AllTest12_CreateUpdate;
import utils.TestUtils;
import top.lingkang.mm.orm.MapperManageImpl;

/**
 * @author lingkang
 * @create by 2024/3/6 14:48
 */
@Slf4j
public class Test05CreateUpdate extends AllTest12_CreateUpdate {
    @BeforeEach
    public void init() {
        sqlSession = TestUtils.getMysql();
        TestUtils.initScript("script/init-mysql.sql", sqlSession);
        mapperManage = new MapperManageImpl(sqlSession.getConfiguration(), sqlSession);
    }
}
