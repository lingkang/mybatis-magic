package test.mysql;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import test.all.AllTest03_Delete;
import top.lingkang.mmt.entity.UserEntityExtends;
import top.lingkang.mmt.mapper.UserEntityExtendsMapper;
import utils.TestUtils;
import top.lingkang.mm.orm.MapperManageImpl;

/**
 * @author lingkang
 * Created by 2024/3/7
 */
@Slf4j
public class Test03Delete extends AllTest03_Delete {
    @BeforeEach
    public void init() {
        sqlSession = TestUtils.getMysql();
        TestUtils.initScript("script/init-mysql.sql", sqlSession);
        mapperManage = new MapperManageImpl(sqlSession.getConfiguration(), sqlSession);
    }

    @Test
    public void delete05() {
        sqlSession.getConfiguration().addMapper(UserEntityExtendsMapper.class);
        UserEntityExtendsMapper mapper = mapperManage.getMapper(UserEntityExtendsMapper.class);
        UserEntityExtends entity = new UserEntityExtends();
        entity.setId(1L);
        mapper.deleteById(entity);
    }

}
