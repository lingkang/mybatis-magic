package test.mysql;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import test.all.AllTest01_Select;
import utils.TestUtils;
import top.lingkang.mm.orm.MapperManageImpl;

/**
 * @Author lingkang
 * @Date 2024/3/1 9:32
 */
@Slf4j
public class Test01Select extends AllTest01_Select {
    @BeforeEach
    public void init() {
        sqlSession = TestUtils.getMysql();
        TestUtils.initScript("script/init-mysql.sql", sqlSession);
        mapperManage = new MapperManageImpl(sqlSession.getConfiguration(), sqlSession);
    }


}
