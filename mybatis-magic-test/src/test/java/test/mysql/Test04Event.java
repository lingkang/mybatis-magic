package test.mysql;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import test.all.AllTest05_Event;
import utils.TestUtils;
import top.lingkang.mm.orm.MapperManageImpl;

/**
 * @author lingkang
 * Created by 2024/3/2
 */
@Slf4j
public class Test04Event extends AllTest05_Event {
    @BeforeEach
    public void init() {
        sqlSession = TestUtils.getMysql();
        TestUtils.initScript("script/init-mysql.sql", sqlSession);
        mapperManage = new MapperManageImpl(sqlSession.getConfiguration(), sqlSession);
    }
}
