package test.mysql;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import test.all.AllTest21_Mapper;
import top.lingkang.mm.orm.MapperManageImpl;
import utils.TestUtils;

/**
 * @author lingkang
 * @create by 2024/3/6 14:48
 */
@Slf4j
public class Test09Mapper extends AllTest21_Mapper {
    @BeforeEach
    public void init() {
        sqlSession = TestUtils.getMysql();
        TestUtils.initScript("script/init-mysql.sql", sqlSession);
        mapperManage = new MapperManageImpl(sqlSession.getConfiguration(), sqlSession);
    }
}
