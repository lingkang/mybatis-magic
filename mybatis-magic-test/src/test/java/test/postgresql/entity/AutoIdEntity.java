package test.postgresql.entity;

import lombok.Data;
import top.lingkang.mm.annotation.Id;
import top.lingkang.mm.annotation.Table;
import top.lingkang.mm.constant.IdType;

/**
 * @author lingkang
 * @create by 2024/3/11 16:35
 */
@Data
@Table("t_autoid")
public class AutoIdEntity {
    @Id(value = IdType.AUTO, sequence = "t_autoid_id_seq")
    private String id;

    private String name;
}
