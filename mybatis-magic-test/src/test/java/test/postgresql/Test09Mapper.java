package test.postgresql;

import org.junit.jupiter.api.BeforeEach;
import test.all.AllTest21_Mapper;
import utils.TestUtils;
import top.lingkang.mm.orm.MapperManageImpl;

/**
 * @author lingkang
 * Created by 2024/3/14
 */
public class Test09Mapper extends AllTest21_Mapper {
    @BeforeEach
    public void init() {
        sqlSession = TestUtils.getPostgreSql();
        TestUtils.initScript("script/init-pgsql.sql", sqlSession);
        mapperManage = new MapperManageImpl(sqlSession.getConfiguration(), sqlSession);
    }
}
