package test.postgresql;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import test.all.AllTest06_Other;
import utils.TestUtils;
import top.lingkang.mm.orm.MapperManageImpl;

/**
 * @author lingkang
 * @create by 2024/3/11 14:48
 */
@Slf4j
public class Test06Other extends AllTest06_Other {
    @BeforeEach
    public void init() {
        sqlSession = TestUtils.getPostgreSql();
        TestUtils.initScript("script/init-pgsql.sql", sqlSession);
        mapperManage = new MapperManageImpl(sqlSession.getConfiguration(), sqlSession);
    }

}
