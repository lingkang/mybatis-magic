package test.postgresql;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import test.all.AllTest02_Insert;
import utils.TestUtils;
import top.lingkang.mm.orm.MapperManageImpl;
import top.lingkang.mm.transaction.TransactionManage;
import top.lingkang.mmt.entity.UserEntity;

/**
 * @author lingkang
 * @create by 2024/3/11 14:48
 */
@Slf4j
public class Test02Insert extends AllTest02_Insert {
    @BeforeEach
    public void init() {
        sqlSession = TestUtils.getPostgreSql();
        TestUtils.initScript("script/init-pgsql.sql", sqlSession);
        mapperManage = new MapperManageImpl(sqlSession.getConfiguration(), sqlSession);
    }

    @Test
    public void insert05() {
        UserEntity entity = new UserEntity();
        entity.setId(System.currentTimeMillis());
        entity.setUsername("lingkang");
        TransactionManage.beginTransaction();
        int insert = mapperManage.insert(entity, false);
        TransactionManage.commit();
        log.info("影响的行数： {} , 结果: {}", insert, entity);
    }
}
