package test.postgresql;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import test.all.AllTest01_Select;
import utils.TestUtils;
import top.lingkang.mm.orm.MapperManageImpl;
import top.lingkang.mmt.mapper.UserMapper;

/**
 * @author lingkang
 * @create by 2024/3/11 15:41
 */
@Slf4j
public class Test01Select extends AllTest01_Select {
    @BeforeEach
    public void init() {
        sqlSession = TestUtils.getPostgreSql();
        TestUtils.initScript("script/init-pgsql.sql", sqlSession);
        mapperManage = new MapperManageImpl(sqlSession.getConfiguration(), sqlSession);
    }

    @Test
    public void test01() throws Exception {
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        System.out.println(mapper.selectAll());
        mapper.selectAll();
        mapper.selectAll();
        mapper.selectAll();
        mapper.selectAll();
    }
}
