package test.postgresql;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import test.postgresql.entity.AutoIdEntity;
import utils.TestUtils;
import top.lingkang.mm.orm.MapperManage;
import top.lingkang.mm.orm.MapperManageImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lingkang
 * @create by 2024/3/11 16:32
 */
@Slf4j
public class Test00AutoId {
    protected SqlSession sqlSession;
    protected MapperManage mapperManage;

    @BeforeEach
    public void init() {
        sqlSession = TestUtils.getPostgreSql();
        TestUtils.initScript("script/init-pgsql.sql", sqlSession);
        mapperManage = new MapperManageImpl(sqlSession.getConfiguration(), sqlSession);
    }

    @Test
    public void autoId() {
        AutoIdEntity autoId = new AutoIdEntity();
        mapperManage.insert(autoId);
        log.info("插入返回id：{}", autoId);

        autoId = new AutoIdEntity();
        autoId.setId(11L+"");
        mapperManage.insert(autoId);
        log.info("2. 插入返回id：{}", autoId);

        autoId = new AutoIdEntity();
        mapperManage.insert(autoId);
        log.info("3. 插入返回id：{}", autoId);
    }

    @Test
    public void autoId2() {
        AutoIdEntity autoId = new AutoIdEntity();
        mapperManage.insert(autoId);
        log.info("插入返回id：{}", autoId);

        List<AutoIdEntity> list = new ArrayList<>();
        autoId = new AutoIdEntity();
        autoId.setId(11L+"");
        list.add(new AutoIdEntity());
        list.add(autoId);
        list.add(new AutoIdEntity());
        mapperManage.insertBatch(list);
        log.info("2. 插入返回id：{}", list);
    }

    @Test
    public void autoId3() {
        AutoIdEntity autoId = new AutoIdEntity();
        mapperManage.insert(autoId, false);
        log.info("插入返回id：{}", autoId);
    }

    @Test
    public void autoId4() {
        AutoIdEntity autoId = new AutoIdEntity();
        autoId.setId(95L+"");
        mapperManage.insert(autoId, false);
        log.info("插入返回id：{}", autoId);
    }

}
