DROP TABLE t_autoid;
CREATE TABLE t_autoid
(
    id     varchar(50)        NOT NULL,
    name varchar(20) NULL,
    CONSTRAINT t_autoid_pk PRIMARY KEY (id)
);
DROP
SEQUENCE t_autoid_id_seq;
CREATE
SEQUENCE t_autoid_id_seq;

DROP TABLE IF EXISTS t_autoid_assign;
CREATE TABLE t_autoid_assign
(
    id varchar(50) PRIMARY KEY,
    name    varchar(20)  NULL
);

DROP TABLE t_order;
CREATE TABLE t_order
(
    id          varchar(50) NOT NULL,
    user_id     int8        NULL,
    status      varchar(2)  NULL,
    create_time timestamp   NULL,
    update_time timestamp   NULL,
    CONSTRAINT t_order_pk PRIMARY KEY (id)
);

INSERT INTO t_order
VALUES ('1708709054284', 1, '2', '2024-02-23 01:24:14', '2024-02-23 01:24:14');

DROP TABLE t_user;
CREATE TABLE t_user
(
    id          BIGINT       NOT NULL,
    username    varchar(20) NULL,
    "password"  varchar(64) NULL,
    nickname    varchar(12) NULL,
    sex         varchar(2)  NULL,
    create_time timestamp   NULL,
    update_time timestamp   NULL,
    CONSTRAINT t_user_unique UNIQUE (id)
);


INSERT INTO t_user
VALUES (1, NULL, NULL, NULL, NULL, '2024-02-23 02:19:31', '2024-02-23 02:33:47'),
       (19951219, NULL, NULL, NULL, NULL, NULL, NULL),
       (1761052961150074880, '1708702719374', '0433921f68964377a7af498f35f1da1b', 'insertAutoId', NULL, NULL, NULL),
       (1761052961393344512, NULL, NULL, NULL, NULL, NULL, NULL),
       (1761053296392404992, '1708702799290', '08197eb46a284162a9fcf01c5c2b274c', 'insertAutoId', NULL, NULL, NULL),
       (1761056584819015680, '1708703583312', '2fdc113037a8491494962ac8b52127b3', 'insertAutoId', NULL, NULL, NULL),
       (1761056693195636736, '1708703609153', 'ef82181997d94fd381dec3ff8f2b7696', 'insertAutoId', NULL, NULL, NULL),
       (1761068137131802624, '1708706337600', '22ceee7580114c87903137b9cdba7a32', 'insertAutoId', NULL, NULL, NULL),
       (1761097741947109376, '1708713395938', '7a13a66532ba46ed9511e5a977d2a7ff', 'insertAutoId', NULL,
        '2024-02-23 02:36:36', '2024-02-23 02:36:36');

