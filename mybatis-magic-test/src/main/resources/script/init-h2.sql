DROP TABLE IF EXISTS `t_order`;

CREATE TABLE IF NOT EXISTS t_order
(
    `id`          varchar(50) PRIMARY KEY,
    `user_id`     BIGINT,
    `status`      varchar(2),
    `create_time` timestamp,
    `update_time` timestamp
);
INSERT INTO `t_order`
VALUES ('1708709054284', 1, '2', '2024-02-23 01:24:14', '2024-02-23 01:24:14');

DROP TABLE IF EXISTS `t_user`;
CREATE TABLE IF NOT EXISTS `t_user`
(
    `id`          BIGINT PRIMARY KEY,
    `username`    varchar(20)    DEFAULT NULL,
    `password`    varchar(64)    DEFAULT NULL,
    `nickname`    varchar(12)    DEFAULT NULL,
    `sex`         varchar(2)     DEFAULT NULL,
    `create_time` timestamp NULL DEFAULT NULL,
    `update_time` timestamp NULL DEFAULT NULL
);

INSERT INTO `t_user`
VALUES (1, NULL, NULL, NULL, NULL, '2024-02-23 02:19:31', '2024-02-23 02:33:47'),
       (19951219, NULL, NULL, NULL, NULL, NULL, NULL),
       (1761052961150074880, '1708702719374', '0433921f68964377a7af498f35f1da1b', 'insertAutoId', NULL, NULL, NULL),
       (1761052961393344512, NULL, NULL, NULL, NULL, NULL, NULL),
       (1761053296392404992, '1708702799290', '08197eb46a284162a9fcf01c5c2b274c', 'insertAutoId', NULL, NULL, NULL),
       (1761056584819015680, '1708703583312', '2fdc113037a8491494962ac8b52127b3', 'insertAutoId', NULL, NULL, NULL),
       (1761056693195636736, '1708703609153', 'ef82181997d94fd381dec3ff8f2b7696', 'insertAutoId', NULL, NULL, NULL),
       (1761068137131802624, '1708706337600', '22ceee7580114c87903137b9cdba7a32', 'insertAutoId', NULL, NULL, NULL),
       (1761097741947109376, '1708713395938', '7a13a66532ba46ed9511e5a977d2a7ff', 'insertAutoId', NULL,
        '2024-02-23 02:36:36', '2024-02-23 02:36:36');

DROP TABLE IF EXISTS `t_autoid`;
CREATE TABLE `t_autoid`
(
    `id` bigint AUTO_INCREMENT PRIMARY KEY,
    `name`    varchar(20)    DEFAULT NULL
);

DROP TABLE IF EXISTS `t_autoid_assign`;
CREATE TABLE `t_autoid_assign`
(
    `id` varchar(50) PRIMARY KEY,
    `name`    varchar(20)    DEFAULT NULL
);