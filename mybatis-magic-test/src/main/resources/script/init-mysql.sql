-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: test
-- ------------------------------------------------------
-- Server version	8.0.36

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `t_autoid`
--

DROP TABLE IF EXISTS `t_autoid`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_autoid`
(
    `id` varchar(50) NOT NULL,
    `name`      varchar(20)       DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `t_autoid_assign`;
CREATE TABLE `t_autoid_assign`
(
    `id` varchar(50) PRIMARY KEY,
    `name`    varchar(20)    DEFAULT NULL
);

--
-- Dumping data for table `t_autoid`
--

LOCK TABLES `t_autoid` WRITE;
/*!40000 ALTER TABLE `t_autoid`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `t_autoid`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_order`
--

DROP TABLE IF EXISTS `t_order`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_order`
(
    `id`          varchar(50) NOT NULL,
    `user_id`     bigint           DEFAULT NULL,
    `status`      varchar(2)       DEFAULT NULL,
    `create_time` timestamp   NULL DEFAULT NULL,
    `update_time` timestamp   NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_order`
--

LOCK TABLES `t_order` WRITE;
/*!40000 ALTER TABLE `t_order`
    DISABLE KEYS */;
INSERT INTO `t_order`
VALUES ('1708709054284', 1, '2', '2024-02-23 01:24:14', '2024-02-23 01:24:14');
/*!40000 ALTER TABLE `t_order`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_user`
--

DROP TABLE IF EXISTS `t_user`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_user`
(
    `id`          BIGINT    NOT NULL,
    `username`    varchar(20)    DEFAULT NULL,
    `password`    varchar(64)    DEFAULT NULL,
    `nickname`    varchar(12)    DEFAULT NULL,
    `sex`         varchar(2)     DEFAULT NULL,
    `create_time` timestamp NULL DEFAULT NULL,
    `update_time` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_user`
--

LOCK TABLES `t_user` WRITE;
/*!40000 ALTER TABLE `t_user`
    DISABLE KEYS */;
INSERT INTO `t_user`
VALUES (1, NULL, NULL, NULL, NULL, '2024-02-23 02:19:31', '2024-02-23 02:33:47'),
       (19951219, NULL, NULL, NULL, NULL, NULL, NULL),
       (1761052961150074880, '1708702719374', '0433921f68964377a7af498f35f1da1b', 'insertAutoId', NULL, NULL, NULL),
       (1761052961393344512, NULL, NULL, NULL, NULL, NULL, NULL),
       (1761053296392404992, '1708702799290', '08197eb46a284162a9fcf01c5c2b274c', 'insertAutoId', NULL, NULL, NULL),
       (1761056584819015680, '1708703583312', '2fdc113037a8491494962ac8b52127b3', 'insertAutoId', NULL, NULL, NULL),
       (1761056693195636736, '1708703609153', 'ef82181997d94fd381dec3ff8f2b7696', 'insertAutoId', NULL, NULL, NULL),
       (1761068137131802624, '1708706337600', '22ceee7580114c87903137b9cdba7a32', 'insertAutoId', NULL, NULL, NULL),
       (1761097741947109376, '1708713395938', '7a13a66532ba46ed9511e5a977d2a7ff', 'insertAutoId', NULL,
        '2024-02-23 02:36:36', '2024-02-23 02:36:36');
/*!40000 ALTER TABLE `t_user`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'test'
--
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2024-02-27 16:05:51

-- test.t_autoid definition
