package demo2;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DatabaseMetaData;

/**
 * @author lingkang
 * @create by 2024/3/11 12:28
 */
public class Demo03PostgreSql {
    public static void main(String[] args) throws Exception{
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:postgresql://10.8.4.191:5432/test");
        config.setUsername("postgres");
        config.setPassword("123456");
        config.setMaximumPoolSize(1);
        DataSource dataSource = new HikariDataSource(config);
        Connection connection = dataSource.getConnection();
        DatabaseMetaData metaData = connection.getMetaData();
        System.out.println(metaData);
        System.out.println(metaData.getURL());
        System.out.println(metaData.getDatabaseProductName());
        connection.close();
        ((HikariDataSource)dataSource).close();
    }
}
