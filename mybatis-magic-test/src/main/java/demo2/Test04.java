package demo2;

import java.io.File;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * @author lingkang
 * @create by 2024/3/18 14:14
 */
public class Test04 {
    public static void main(String[] args) throws Exception {
        String scanPath = "mapper";
        List<String> result = scanResource(scanPath);
        // 输出扫描结果
        System.out.println(result);
        // 加载配置
        /*for (String config:result){
            InputStream inputStream = Test04.class.getClassLoader().getResourceAsStream(config);
        }*/
    }

    public static List<String> scanResource(String scanPath) throws Exception {
        URL url = Test04.class.getClassLoader().getResource(scanPath);
        List<String> result = new ArrayList<>();
        if (url != null) {
            JarFile jarFile = null;
            URLConnection con = url.openConnection();
            if (con instanceof JarURLConnection) {
                JarURLConnection jarCon = (JarURLConnection) con;
                jarFile = jarCon.getJarFile();
            } else {
                // 手动接收结果
                String urlFile = url.getFile();
                int separatorIndex = urlFile.indexOf("*/");// tomcat
                if (separatorIndex == -1) {
                    separatorIndex = urlFile.indexOf("!/");// jar
                }
                if (separatorIndex != -1) {
                    // String jarFileUrl = urlFile.substring(0, separatorIndex);
                    String rootEntryPath = urlFile.substring(separatorIndex + 2);  // both separators are 2 chars
                    jarFile = new JarFile(rootEntryPath);
                }
            }

            // 遍历
            if (jarFile != null) {
                boolean has = false;
                for (Enumeration<JarEntry> entries = jarFile.entries(); entries.hasMoreElements(); ) {
                    JarEntry entry = entries.nextElement();
                    String entryPath = entry.getName();
                    // 名称匹配，可以是 ant、正则
                    if (entryPath.startsWith(scanPath)) {
                        result.add(entryPath);
                        has = true;
                    } else {
                        if (has) {
                            break;
                        }
                    }
                }
                jarFile.close();
            } else {
                // 直接遍历，此时可能是idea、eclipse开发环境。
                URL resource = Test04.class.getClassLoader().getResource("");
                if (resource != null) {
                    File file = new File(resource.getPath() + scanPath);
                    if (file.listFiles() != null)
                        for (File f : file.listFiles()) {
                            result.add(f.getPath());
                        }
                }
            }
        }
        return result;
    }
}
