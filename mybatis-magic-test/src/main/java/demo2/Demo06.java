package demo2;

import cn.hutool.core.io.FileUtil;

import java.io.File;
import java.util.List;

/**
 * @author lingkang
 * Created by 2024/5/3
 */
public class Demo06 {
    public static void main(String[] args) {
        System.out.println(FileUtil.getAbsolutePath("mapper/**"));
        System.out.println(FileUtil.getAbsolutePath("mapper/*"));
        System.out.println(FileUtil.getAbsolutePath("mapper/*.xml"));
        System.out.println(FileUtil.getAbsolutePath("mapper/**.xml"));
        System.out.println(FileUtil.getAbsolutePath("mapper/"));
        List<File> list = FileUtil.loopFiles("mapper/**");
        System.out.println(list);
    }
}
