package demo2;

import cn.hutool.core.util.ClassUtil;
import top.lingkang.mm.annotation.PostUpdate;
import top.lingkang.mm.annotation.PreUpdate;
import top.lingkang.mm.utils.MagicUtils;
import top.lingkang.mmt.entity.UserEntityExtends;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @author lingkang
 * @create by 2024/3/6 17:14
 */
public class Demo01 {
    public static void main(String[] args) {
        Method[] publicMethods = ClassUtil.getPublicMethods(UserEntityExtends.class);
        for (Method method : publicMethods)
            if (method.isAnnotationPresent(PostUpdate.class) || method.isAnnotationPresent(PreUpdate.class))
                System.out.println(method.getName());

        Field[] fields = MagicUtils.getAllField(UserEntityExtends.class);
        for (Field field : fields)
            System.out.println(field.getName());

    }
}
