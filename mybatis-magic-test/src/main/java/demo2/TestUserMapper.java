package demo2;

import top.lingkang.mm.orm.BaseMapper;
import top.lingkang.mmt.entity.UserEntity;

import java.util.List;

/**
 * @author lingkang
 * @create by 2024/3/18 9:47
 */
public interface TestUserMapper extends BaseMapper<UserEntity> {
    List<UserEntity> list();
}
