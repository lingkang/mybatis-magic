package demo2;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lingkang
 * @create by 2024/3/18 14:44
 */
public class Test05 {
    public static void main(String[] args) {
        String scanPath="mapper";
        List<String> result=new ArrayList<>();
        // 直接遍历，此时可能是idea、eclipse开发环境。
        URL resource = Test04.class.getClassLoader().getResource("");
        if (resource != null) {
            File file = new File(resource.getPath() + scanPath);
            if (file.listFiles() != null)
                for (File f : file.listFiles()) {
                    result.add(f.getPath());
                }
        }
        // 输出扫描结果
        System.out.println(result);
        // 加载配置
        /*for (String config:result){
            InputStream inputStream = Test04.class.getClassLoader().getResourceAsStream(config);
        }*/
    }
}
