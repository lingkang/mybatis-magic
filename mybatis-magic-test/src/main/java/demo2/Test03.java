package demo2;

import org.apache.ibatis.session.SqlSession;
import utils.TestUtils;

/**
 * @author lingkang
 * @create by 2024/3/18 9:48
 */
public class Test03 {
    public static void main(String[] args) throws Exception {
        SqlSession session = TestUtils.getH2();
        TestUtils.initScript("script/init-h2.sql", session);
        TestUserMapper mapper = session.getMapper(TestUserMapper.class);
        System.out.println(mapper.list());
        System.out.println(mapper.selectAll());
    }
}
