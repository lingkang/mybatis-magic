package demo2;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.security.CodeSource;
import java.security.ProtectionDomain;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class JarPathFinder {
    public static void main(String[] args) throws Exception {
        String dir = "mapper";
        List<String> list=new ArrayList<>();
        try (FileInputStream fis = new FileInputStream("C:\\Users\\Administrator\\Desktop\\project\\git\\mybatis-magic\\out\\artifacts\\mybatis_magic_test_jar\\mybatis-magic-test.jar");
             ZipInputStream zis = new ZipInputStream(fis)) {

            ZipEntry entry;
            boolean has = false;
            while ((entry = zis.getNextEntry()) != null) {
                if (!entry.getName().startsWith(dir)) {
                    if (has) {
                        zis.closeEntry();
                        break;
                    } else
                        continue;
                }
                list.add(entry.getName());

                System.out.println("文件名: " + entry.getName());
                has = true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getJarPath() {
        try {
            ProtectionDomain protectionDomain = JarPathFinder.class.getProtectionDomain();
            CodeSource codeSource = protectionDomain.getCodeSource();
            if (codeSource != null) {
                URL location = codeSource.getLocation();
                if (location != null) {
                    File jarFile = new File(location.toURI());
                    if (jarFile.getAbsolutePath().endsWith(".jar"))
                        return jarFile.getAbsolutePath();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}