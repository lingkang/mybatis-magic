package testMethod;

import top.lingkang.mm.orm.Query;

/**
 * @author lingkang
 * @create by 2024/3/12 18:03
 */
public class Test01 {
    public static void main(String[] args) {
        Query query = new Query();
        query.orderByDesc("id").orderByDesc("username")
                .orderByDesc("password", "nickname").orderByAsc("create_time", "sex");
        System.out.println(query.buildSql());

        query = new Query();
        query.orderByDesc("id", "update_time").orderByDesc("username")
                .orderByDesc("password", "nickname").orderByAsc("create_time", "sex");
        System.out.println(query.buildSql());
    }
}
