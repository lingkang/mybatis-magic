package testMethod;

import top.lingkang.mm.orm.Query;

/**
 * @author lingkang
 * @create by 2024/3/12 18:03
 */
public class Test02 {
    public static void main(String[] args) {
        Query query = new Query();
        query.eq("id",123);
        query.eq("username","lk").or().eq("password","123");
        System.out.println(query.buildSql());
    }
}
