package utils;

import cn.hutool.core.io.IoUtil;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import top.lingkang.mm.MagicConfiguration;
import top.lingkang.mm.utils.MagicUtils;

import java.nio.charset.StandardCharsets;
import java.sql.Connection;

/**
 * @author lingkang
 * @create by 2024/3/11 14:55
 */
public class TestUtils {
    public static SqlSession getMysql() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:mysql://localhost:3306/test?useSSL=true");
        config.setUsername("root");
        config.setPassword("123456");
        config.setMaximumPoolSize(1);
        HikariDataSource dataSource = new HikariDataSource(config);

        MagicConfiguration configuration = new MagicConfiguration(dataSource);
        configuration.setMapUnderscoreToCamelCase(true);
        configuration.addMapperXml("mapper");
        SqlSessionFactory sessionFactory = configuration.build();
        return sessionFactory.openSession();
    }

    public static void initScript(String sqlPath, SqlSession sqlSession) {
        Connection connection = sqlSession.getConnection();
        String script = IoUtil.read(TestUtils.class.getClassLoader().getResourceAsStream(sqlPath), StandardCharsets.UTF_8);
        MagicUtils.exeScript(script, connection);
    }

    public static SqlSession getH2() {
        HikariConfig config = new HikariConfig();
        config.setDriverClassName("org.h2.Driver");
        config.setJdbcUrl("jdbc:h2:mem:mybatismagicdb");
        config.setUsername("root");
        config.setPassword("123456");
        config.setMaximumPoolSize(1);
        HikariDataSource dataSource = new HikariDataSource(config);

        MagicConfiguration configuration = new MagicConfiguration(dataSource);
        configuration.setMapUnderscoreToCamelCase(true);
        configuration.addMapperXml("mapper");
        SqlSessionFactory sessionFactory = configuration.build();
        return sessionFactory.openSession();
    }

    public static SqlSession getPostgreSql() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:postgresql://10.0.0.103:5432/test");
        config.setUsername("postgres");
        config.setPassword("123456");
        config.setMaximumPoolSize(1);
        HikariDataSource dataSource = new HikariDataSource(config);

        MagicConfiguration configuration = new MagicConfiguration(dataSource);
        configuration.setMapUnderscoreToCamelCase(true);
        configuration.addMapperXml("mapper");
        SqlSessionFactory sessionFactory = configuration.build();
        return sessionFactory.openSession();
    }

    public static SqlSession getSqlite() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:sqlite:d://sqlite-mybatis-magic.db");
        config.setUsername("postgres");
        config.setPassword("123456");
        config.setMaximumPoolSize(1);
        HikariDataSource dataSource = new HikariDataSource(config);

        MagicConfiguration configuration = new MagicConfiguration(dataSource);
        // configuration.setMapUnderscoreToCamelCase(true);
        configuration.addMapperXml("mapper");
        SqlSessionFactory sessionFactory = configuration.build();
        return sessionFactory.openSession();
    }


}
