package test.magicmapper;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import top.lingkang.mm.MagicConfiguration;
import top.lingkang.mmt.entity.IdEntityAssign;
import top.lingkang.mmt.mapper.IdEntityAssignMapper;

/**
 * @author lingkang
 * @create by 2024/3/12 9:40
 */
public class Test01 {
    public static void main(String[] args) {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:mysql://localhost:3306/test?useSSL=true&serverTimezone=UTC");
        config.setUsername("root");
        config.setPassword("123456");
        config.setMaximumPoolSize(3);
        HikariDataSource dataSource = new HikariDataSource(config);


        MagicConfiguration configuration = new MagicConfiguration(dataSource);
        configuration.setMapUnderscoreToCamelCase(true);// 下划线转化驼峰
        /*configuration.addMapper(UserMapper.class);
        configuration.addMapper(OrderBaseMapper.class);
        configuration.addMapper(UserBaseMapper.class);*/
        configuration.addMappers("test.magicmapper");
        SqlSessionFactory sessionFactory = configuration.build();
        SqlSession session = sessionFactory.openSession();
        /*UserBaseMapper mapper = session.getMapper(UserBaseMapper.class);
        System.out.println(mapper.selectAll());

        OrderBaseMapper mapper1 = session.getMapper(OrderBaseMapper.class);
        System.out.println(mapper1.selectAll());

        UserEntity entity = new UserEntity();
        entity.setPassword(System.currentTimeMillis() + "");
        entity.setId(System.currentTimeMillis());
        mapper.insert(entity);
        System.out.println(entity);*/

        IdEntityAssign assign = new IdEntityAssign();
        IdEntityAssignMapper mapper2 = session.getMapper(IdEntityAssignMapper.class);
        mapper2.insert(assign);
        System.out.println(assign);
    }
}
