package test.magicmapper;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import top.lingkang.mm.MagicConfiguration;
import top.lingkang.mm.orm.Query;
import top.lingkang.mmt.entity.IdEntityAssign;
import top.lingkang.mmt.entity.UserEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lingkang
 * @create by 2024/3/12 9:40
 */
public class Test02 {
    public static void main(String[] args) {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:mysql://localhost:3306/test?useSSL=true&serverTimezone=UTC");
        config.setUsername("root");
        config.setPassword("123456");
        config.setMaximumPoolSize(1);
        HikariDataSource dataSource = new HikariDataSource(config);


        MagicConfiguration configuration = new MagicConfiguration(dataSource);
        configuration.setMapUnderscoreToCamelCase(true);// 下划线转化驼峰
        /*configuration.addMapper(UserMapper.class);
        configuration.addMapper(OrderBaseMapper.class);
        configuration.addMapper(UserBaseMapper.class);*/
        configuration.addMappers("test.magicmapper");
        SqlSessionFactory sessionFactory = configuration.build();
        SqlSession session = sessionFactory.openSession();
        UserBaseMapper mapper = session.getMapper(UserBaseMapper.class);
        List<Long> ids=new ArrayList<>();
        ids.add(1L);
        ids.add(19951219L);
        List<UserEntity> list = mapper.createQuery(new Query().in("id",ids ).orderByDesc("id"));
        System.out.println(list);
    }
}
