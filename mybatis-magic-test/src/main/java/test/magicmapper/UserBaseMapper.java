package test.magicmapper;

import top.lingkang.mm.annotation.MagicMapper;
import top.lingkang.mm.orm.BaseMapper;
import top.lingkang.mmt.entity.UserEntity;

/**
 * @author lingkang
 * @create by 2024/3/12 9:40
 */
@MagicMapper
public interface UserBaseMapper extends BaseMapper<UserEntity> {
}
