package testload;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSessionFactory;
import top.lingkang.mm.MagicConfiguration;

/**
 * @author lingkang
 * Created by 2024/5/3
 */
@Slf4j
public class TestLoad01 {
    public static void main(String[] args) throws Exception {
        Thread.sleep(6000);
        HikariConfig config = new HikariConfig();
        config.setDriverClassName("org.h2.Driver");
        config.setJdbcUrl("jdbc:h2:mem:mybatismagicdb");
        config.setUsername("root");
        config.setPassword("123456");
        config.setMaximumPoolSize(1);
        HikariDataSource dataSource = new HikariDataSource(config);

        MagicConfiguration configuration = new MagicConfiguration(dataSource);
        configuration.setMapUnderscoreToCamelCase(true);
        configuration.addMapperXml("mapper");
        configuration.addMapperXml("mapper/**.xml");
        SqlSessionFactory sessionFactory = configuration.build();
    }
}
