package demo3;

import cn.hutool.core.text.NamingCase;

import java.lang.reflect.Field;

/**
 * @author lingkang
 * Create by 2024/11/18 8:48
 */
public class Demo31 {
    public static void main(String[] args) {
        System.out.println(NamingCase.toUnderlineCase("createTime"));
        DemoBean31 bean=new DemoBean31();
        Field[] fields = DemoBean31.class.getDeclaredFields();
        for (Field field : fields) {
            System.out.println(field.getName());
            System.out.println(field.getType());
        }
    }
}
