package demo3;

import lombok.Data;

/**
 * @author lingkang
 * Create by 2024/11/18 8:49
 */
@Data
public class DemoBean31 {
    private int id;
    private String name;
}
