package security;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class GenId implements Callable<List<Long>> {
    private
    Snowflake snowflake = IdUtil.getSnowflake(0, 1);

    @Override
    public List<Long> call() throws Exception {
        List<Long> res = new ArrayList<>(1024);
        for (int i = 0; i < 600; i++)
            res.add(snowflake.nextId());
        return res;
    }
}
