package security;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author lingkang
 * @create by 2024/5/27 14:51
 */
public class Test01 {
    public static void main(String[] args) throws Exception {
        ExecutorService executorService = Executors.newFixedThreadPool(32); // 创建一个固定大小的线程池
        List<Future<List<Long>>> futures = new ArrayList<>();

        for (int i = 0; i < 64; i++) {
            Future<List<Long>> future = executorService.submit(new GenId()); // 提交任务并获取Future对象
            futures.add(future);
        }

        HashSet<Long> has = new HashSet<>();
        int count = 0;
        // 收集结果
        for (Future<List<Long>> future : futures) {
            List<Long> result = future.get(); // 获取结果，这可能会阻塞直到结果可用
            for (Long val : result) {
                if (has.contains(val)) {
                    // System.out.println("存在重复的id --> " + val);
                    count++;
                    continue;
                }
                has.add(val);
            }
        }
        System.out.println("重复id数量 --> " + count);
        System.out.println(has.size());

        executorService.shutdown();
    }

}

