package security;

import cn.hutool.core.lang.UUID;

/**
 * @author lingkang
 * @create by 2024/5/27 15:25
 */
public class Test02 {
    public static void main(String[] args) {
        System.out.println(System.currentTimeMillis());
        UUID uuid = UUID.fastUUID();
        System.out.println(uuid);
        System.out.println(uuid.toString().replace("-",""));
    }
}
