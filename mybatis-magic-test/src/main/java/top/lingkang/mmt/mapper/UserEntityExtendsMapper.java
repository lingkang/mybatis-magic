package top.lingkang.mmt.mapper;

import top.lingkang.mm.orm.BaseMapper;
import top.lingkang.mmt.entity.UserEntityExtends;

/**
 * @author lingkang
 * @create by 2024/3/28 17:43
 */
public interface UserEntityExtendsMapper extends BaseMapper<UserEntityExtends> {
}
