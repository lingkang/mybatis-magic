package top.lingkang.mmt.mapper;

import top.lingkang.mmt.entity.OrderEntity;

import java.util.List;

/**
 * @author lingkang
 * @create by 2024/3/11 14:01
 */
public interface OrderMapper {
    List<OrderEntity> selectAll();
}
