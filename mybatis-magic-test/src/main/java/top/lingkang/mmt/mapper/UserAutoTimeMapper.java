package top.lingkang.mmt.mapper;

import top.lingkang.mm.orm.BaseMapper;
import top.lingkang.mmt.entity.UserAutoTimeEntity;

/**
 * @author lingkang
 * Create by 2024/11/18 9:09
 */
public interface UserAutoTimeMapper extends BaseMapper<UserAutoTimeEntity> {
}
