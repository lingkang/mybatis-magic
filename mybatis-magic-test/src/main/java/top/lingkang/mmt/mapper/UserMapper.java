package top.lingkang.mmt.mapper;

import org.apache.ibatis.annotations.Select;
import top.lingkang.mm.orm.BaseMapper;
import top.lingkang.mmt.entity.UserEntity;

import java.util.List;
import java.util.Map;

/**
 * @author lingkang
 * Created by 2024/2/28
 */
public interface UserMapper extends BaseMapper<UserEntity> {

    @Select("select * from t_user")
    List<UserEntity> all();
}
