package top.lingkang.mmt.mapper;

import top.lingkang.mm.orm.BaseMapper;
import top.lingkang.mmt.entity.IdEntityAssign;

/**
 * @author lingkang
 * @create by 2024/3/12 14:02
 */
public interface IdEntityAssignMapper extends BaseMapper<IdEntityAssign> {
}
