package top.lingkang.mmt.mapper;

import org.apache.ibatis.annotations.Lang;
import org.apache.ibatis.annotations.Select;
import top.lingkang.mmt.entity.UserEntity;
import top.lingkang.mmt.language.MagicLanguageDriver;

import java.util.List;
import java.util.Map;

/**
 * @author lingkang
 * @create by 2024/3/6 10:00
 */
public interface LanguageDriverMapper {
    @Select("select * from t_user where id>#{id}")
    @Lang(MagicLanguageDriver.class)
    List<UserEntity> selectAll(Map<String, Object> map);
}
