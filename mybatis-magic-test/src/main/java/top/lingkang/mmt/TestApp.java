package top.lingkang.mmt;

import org.noear.solon.Solon;

/**
 * @author lingkang
 * Created by 2024/2/28
 */
public class TestApp {
    public static void main(String[] args) {
        Solon.start(TestApp.class, args, solonApp -> {
            solonApp.enableTransaction(false);
        });
    }
}
