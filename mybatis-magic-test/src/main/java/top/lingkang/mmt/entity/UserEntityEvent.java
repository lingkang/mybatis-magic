package top.lingkang.mmt.entity;

import lombok.Data;
import top.lingkang.mm.annotation.*;

import java.util.Date;

/**
 * @author lingkang
 * Created by 2024/3/2
 */
@Table("t_user")
@Data
public class UserEntityEvent {
    @Id
    private Long id;
    private String username;
    private String password;
    private String nickname;

    @Column("create_time")
    private Date createTime;
    @Column("update_time")
    private Date updateTime;

    @PreUpdate
    public void pre() {
        nickname="@PreUpdate";
        System.out.println("@PreUpdate");
    }

    @PostUpdate
    public void post() {
        System.out.println("@PostUpdate");
    }
}
