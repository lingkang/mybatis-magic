package top.lingkang.mmt.entity;

import lombok.Data;
import top.lingkang.mm.annotation.Column;
import top.lingkang.mm.annotation.PreUpdate;
import top.lingkang.mm.annotation.Table;

import java.util.Date;

/**
 * @author lingkang
 * Created by 2024/2/28
 */
@Table("t_user")
@Data
public class UserNotIdEntity {
    private String id;
    private String username;
    private String password;
    private String nickname;
    @Column("create_time")
    private Date createTime;
    @Column("update_time")
    private Date updateTime;

    @PreUpdate
    public void pre() {
        updateTime = new Date();
        if (createTime == null)
            createTime = updateTime;
    }
}
