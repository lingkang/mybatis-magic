package top.lingkang.mmt.entity;

import lombok.Data;
import top.lingkang.mm.annotation.*;

import java.util.Date;

/**
 * @author lingkang
 * Created by 2024/2/28
 */
@Table("t_user")
@Data
public class UserAutoTimeEntity {
    @Id
    private String id;
    private String username;
    private String password;
    private String nickname;
    @AutoCreateTime
    @Column(toUnderlineCase = true)
    private Date createTime;
    @AutoUpdateTime
    @Column("update_time")
    private Date updateTime;
}
