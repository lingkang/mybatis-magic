package top.lingkang.mmt.entity;

import lombok.Data;
import top.lingkang.mm.annotation.Id;
import top.lingkang.mm.annotation.Table;
import top.lingkang.mm.constant.IdType;

/**
 * @author lingkang
 * Created by 2024/3/1
 */
@Data
@Table("t_autoid_assign")
public class IdEntityAssign {
    @Id(IdType.ASSIGN)
    private String id;
    private String name;
}
