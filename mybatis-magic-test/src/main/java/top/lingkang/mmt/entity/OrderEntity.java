package top.lingkang.mmt.entity;

import lombok.Getter;
import lombok.Setter;
import top.lingkang.mm.annotation.Column;
import top.lingkang.mm.annotation.Id;
import top.lingkang.mm.annotation.Table;

/**
 * @author lingkang
 * @create by 2024/3/11 13:59
 */
@Getter
@Setter
@Table("t_order")
public class OrderEntity extends BaseTime{
    @Id
    private String id;
    @Column("user_id")
    private Long userId;
    private String status;

    @Override
    public String toString() {
        return "OrderEntity{" +
                "id='" + id + '\'' +
                ", userId='" + userId + '\'' +
                ", status='" + status + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
