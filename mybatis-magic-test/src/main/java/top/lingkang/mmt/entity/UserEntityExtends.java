package top.lingkang.mmt.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import top.lingkang.mm.annotation.Id;
import top.lingkang.mm.annotation.PreUpdate;
import top.lingkang.mm.annotation.Table;

import java.util.Date;

/**
 * @author lingkang
 * Created by 2024/2/28
 */
@Table("t_user")
@Getter
@Setter
public class UserEntityExtends extends BaseTime{
    @Id
    private Long id;
    private String username;
    private String password;
    private String nickname;

    @PreUpdate
    public void pre(){
        updateTime=new Date();
    }

    @Override
    public String toString() {
        return "UserEntityExtends{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", nickname='" + nickname + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
