package top.lingkang.mmt.entity;

import lombok.Data;
import top.lingkang.mm.annotation.Id;
import top.lingkang.mm.annotation.Table;

/**
 * @author lingkang
 * Created by 2024/3/1
 */
@Data
@Table("t_autoid")
public class IdEntity {
    @Id
    private String id;
    private String name;
}
