package top.lingkang.mmt.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;
import top.lingkang.mm.MagicConfiguration;

import javax.sql.DataSource;

/**
 * @author lingkang
 * Created by 2024/2/28
 */
@Configuration
public class AppConfig {
    @Bean
    public DataSource dataSource() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:mysql://localhost:3306/test?useSSL=true&serverTimezone=UTC");
        config.setUsername("root");
        config.setPassword("123456");
        return new HikariDataSource(config);
    }

    @Bean
    public SqlSessionFactory sqlSessionFactory(@Inject DataSource dataSource) throws Exception {
        MagicConfiguration magicConfiguration = new MagicConfiguration(dataSource);
        magicConfiguration.addMapperXml("mapper");
        magicConfiguration.setMapUnderscoreToCamelCase(true);

        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(magicConfiguration);
        return sqlSessionFactory;
    }
}
