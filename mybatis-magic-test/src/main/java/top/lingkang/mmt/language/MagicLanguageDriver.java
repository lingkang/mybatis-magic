package top.lingkang.mmt.language;

import org.apache.ibatis.builder.SqlSourceBuilder;
import org.apache.ibatis.executor.parameter.ParameterHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ResultMap;
import org.apache.ibatis.mapping.SqlSource;
import org.apache.ibatis.scripting.xmltags.XMLLanguageDriver;
import top.lingkang.mm.error.MagicException;

import java.lang.reflect.Field;
import java.util.HashMap;

/**
 * @author lingkang
 * @create by 2024/3/6 9:59
 */
public class MagicLanguageDriver extends XMLLanguageDriver {
    @Override
    public ParameterHandler createParameterHandler(MappedStatement mappedStatement, Object parameterObject, BoundSql boundSql) {
        ResultMap resultMap = mappedStatement.getResultMaps().get(0);
        try {
            Field field = ResultMap.class.getDeclaredField("type");
            field.setAccessible(true);
            field.set(resultMap, HashMap.class);
        } catch (Exception e) {
            throw new MagicException(e);
        }
        // SqlSource sqlSource = mappedStatement.getSqlSource();
        return super.createParameterHandler(mappedStatement, parameterObject, boundSql);
    }
}
