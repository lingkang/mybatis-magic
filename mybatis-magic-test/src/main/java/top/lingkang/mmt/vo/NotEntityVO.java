package top.lingkang.mmt.vo;

import lombok.Data;

/**
 * @author lingkang
 * Create by 2024/12/1 5:25
 */
@Data
public class NotEntityVO {
    private Long id;
    private String nickname;
}
