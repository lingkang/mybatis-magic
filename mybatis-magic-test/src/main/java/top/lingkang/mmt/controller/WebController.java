package top.lingkang.mmt.controller;

import org.apache.ibatis.session.SqlSessionFactory;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import top.lingkang.mmt.mapper.UserMapper;

/**
 * @author lingkang
 * Created by 2024/2/28
 */
@Controller
public class WebController {
    @Inject
    private SqlSessionFactory sqlSessionFactory;

    @Mapping("/")
    public Object index() {
        UserMapper mapper = sqlSessionFactory.openSession().getMapper(UserMapper.class);
        System.out.println(mapper.selectAll());
        return "hello";
    }
}
