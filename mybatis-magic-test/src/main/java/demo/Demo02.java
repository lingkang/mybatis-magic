package demo;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import demo.config.MyInterceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import top.lingkang.mm.MagicConfiguration;
import top.lingkang.mm.MagicSqlSession;
import top.lingkang.mm.transaction.TransactionManage;
import top.lingkang.mmt.mapper.UserMapper;

/**
 * @Author lingkang
 * @Date 2024/2/29 16:47
 */
public class Demo02 {
    public static void main(String[] args) {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:mysql://localhost:3306/test?useSSL=true&serverTimezone=UTC");
        config.setUsername("root");
        config.setPassword("123456");
        config.setMaximumPoolSize(3);
        HikariDataSource dataSource = new HikariDataSource(config);

        MagicConfiguration configuration = new MagicConfiguration(dataSource);
        configuration.addMapperXml("mapper");
        configuration.addInterceptor(new MyInterceptor());
        SqlSessionFactory sessionFactory = configuration.build();
        MagicSqlSession session = new MagicSqlSession(sessionFactory, null);

        TransactionManage.beginTransaction();
        UserMapper mapper = session.getMapper(UserMapper.class);
        System.out.println(mapper.selectAll());

        TransactionManage.rollback();

        System.out.println(mapper.selectAll());
    }
}
