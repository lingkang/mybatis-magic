package demo;

import cn.hutool.core.util.ClassUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lingkang
 * @create by 2024/3/6 12:22
 */
public class Demo11 {
    public static void main(String[] args) {
        HashMap map=new HashMap();
        map.put("1",1);
        System.out.println(map.getClass().isAssignableFrom(Map.class));
        System.out.println(map instanceof Map);
    }
}
