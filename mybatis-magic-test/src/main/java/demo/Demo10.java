package demo;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import top.lingkang.mm.MagicConfiguration;

import java.util.List;

/**
 * @author lingkang
 * @create by 2024/3/5 16:06
 */
public class Demo10 {
    public static void main(String[] args) throws Exception {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:mysql://localhost:3306/test?useSSL=true&serverTimezone=UTC");
        config.setUsername("root");
        config.setPassword("123456");
        config.setMaximumPoolSize(3);
        HikariDataSource dataSource = new HikariDataSource(config);


        MagicConfiguration configuration = new MagicConfiguration(dataSource);
        configuration.setMapUnderscoreToCamelCase(true);// 下划线转化驼峰

        SqlSessionFactory sessionFactory = configuration.build();
        SqlSession session = sessionFactory.openSession();

        configuration.addMapper(MyInterface.class);

        List<Object> list = session.selectList("select", null, new RowBounds(0, 99));
        System.out.println(list);
    }
}
