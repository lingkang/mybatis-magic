package demo;

import cn.hutool.core.util.IdUtil;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author lingkang
 * Created by 2024/3/2
 */
public class Demo03 {
    public static void main(String[] args) {
        // 解密雪花算法生成的 id 时间
        long id = 1763603062414376960L;
        long dateTime = IdUtil.getSnowflake(0, 0).getGenerateDateTime(id);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = format.format(new Date(dateTime));
        System.out.println(date);
    }
}
