package demo;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import top.lingkang.mm.MagicConfiguration;
import top.lingkang.mmt.entity.UserEntity;
import top.lingkang.mmt.mapper.UserMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author lingkang
 * @Date 2024/3/4
 */
public class Demo09 {
    public static void main(String[] args) throws Exception {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:mysql://localhost:3306/test?useSSL=true&serverTimezone=UTC");
        config.setUsername("root");
        config.setPassword("123456");
        config.setMaximumPoolSize(3);
        HikariDataSource dataSource = new HikariDataSource(config);


        MagicConfiguration configuration = new MagicConfiguration(dataSource);
        configuration.addMapperXml("mapper/OtherMapper.xml");
        configuration.setMapUnderscoreToCamelCase(true);// 下划线转化驼峰

        SqlSessionFactory sessionFactory = configuration.build();
        SqlSession session = sessionFactory.openSession();
        /*List<UserEntity> list = session.selectList("selectAll");
        System.out.println(list);*/

        Map<String,Object> map=new HashMap<>();
        map.put("sql","select * from t_user where id>#{id}");
        map.put("id",1);
        map.put("resultType","Map");
        List<Object> list1 = session.selectList("selectd", map);
        System.out.println(list1);

    }
}
