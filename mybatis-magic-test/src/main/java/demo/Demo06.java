package demo;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import top.lingkang.mmt.entity.UserEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lingkang
 * Created by 2024/3/3
 */
public class Demo06 {
    public static void main(String[] args) {
        String user = StrUtil.toUnderlineCase("user");
        System.out.println(user);

        long[] a=new long[]{12,2};
        String string = a.toString();
        System.out.println(string);

        Map map=new HashMap();
        map.put(",","123");
        System.out.println(map);

        List list=new ArrayList();
        list.add("asd");
        System.out.println(list);

        List<UserEntity> entities=new ArrayList<>();
        UserEntity entity=new UserEntity();
        entity.setPassword("@123");
        entities.add(entity);
        System.out.println(entity.toString());

        Class<?> cc= Map.class;
        System.out.println(cc.isAssignableFrom(Map.class));
    }
}
