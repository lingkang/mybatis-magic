package demo;

import org.apache.ibatis.annotations.Select;

import java.util.Map;

/**
 * @author lingkang
 * @create by 2024/3/5 16:06
 */
public interface MyInterface {
    @Select("select * from t_user")
    Map<String,Object> select();
}
