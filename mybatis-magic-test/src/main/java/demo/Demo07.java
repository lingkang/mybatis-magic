package demo;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.ibatis.builder.xml.XMLMapperBuilder;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import top.lingkang.mmt.entity.UserEntity;
import top.lingkang.mmt.mapper.UserMapper;

import java.io.InputStream;
import java.util.List;

/**
 * @Author lingkang
 * @Date 2024/3/3
 */
public class Demo07 {
    public static void main(String[] args) throws Exception {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:mysql://localhost:3306/test?useSSL=true&serverTimezone=UTC");
        config.setUsername("root");
        config.setPassword("123456");
        config.setMaximumPoolSize(3);
        HikariDataSource dataSource = new HikariDataSource(config);

        Configuration configuration = new Configuration();

        InputStream inputStream = Resources.getResourceAsStream("mapper/IdEntityAssignMapper.xml");
        XMLMapperBuilder mapperParser = new XMLMapperBuilder(inputStream, configuration, "mapper/IdEntityAssignMapper.xml",
                configuration.getSqlFragments());
        mapperParser.parse();

        inputStream = Resources.getResourceAsStream("mapper/OtherMapper.xml");
        mapperParser = new XMLMapperBuilder(inputStream, configuration, "mapper/OtherMapper.xml",
                configuration.getSqlFragments());
        mapperParser.parse();


        // 配置
        TransactionFactory transactionFactory = new JdbcTransactionFactory();
        Environment environment = new Environment("dev", transactionFactory, dataSource);
        configuration.setEnvironment(environment);
        configuration.setMapUnderscoreToCamelCase(true);// 下划线转化驼峰

        SqlSessionFactory sessionFactory = new SqlSessionFactoryBuilder().build(configuration);

        try (SqlSession session = sessionFactory.openSession()) {
            List<UserEntity> list = session.selectList("other.selectAll");
            UserEntity user = list.get(0);
            int update = session.update("other.hump", user);
            System.out.println("受影响的行: " + update);
            UserMapper mapper = session.getMapper(UserMapper.class);
            System.out.println(mapper.selectAll());
        }
    }
}
