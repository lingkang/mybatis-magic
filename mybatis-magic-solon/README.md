# mybatis-magic-solon

`mybatis-magic` 的 `solon` 实现。

## 1. 依赖

`solon 2.6 +`

```xml
<dependency>
    <groupId>top.lingkang</groupId>
    <artifactId>mybatis-magic-solon</artifactId>
    <version>1.1.2</version>
</dependency>
```
## 2. 配置

Bean 配置：
```java
@Configuration
public class AppConfig {
    @Bean
    public DataSource dataSource() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:mysql://localhost:3306/test?useSSL=true&serverTimezone=UTC");
        config.setUsername("root");
        config.setPassword("123456");
        return new HikariDataSource(config);
    }

    @Bean
    public SqlSessionFactory sqlSessionFactory(@Inject DataSource dataSource) {
        SolonMagicConfiguration configuration = new SolonMagicConfiguration(dataSource);
        // 推荐开启
        configuration.setMapUnderscoreToCamelCase(true);// 下划线转驼峰
        
        // 文件加载 resources/mapper/UserMapper.xml
        // configuration.addMapperXml("mapper/UserMapper.xml");
        // 或者
        configuration.addMapperXml("mapper/**.xml");
        // 或者直接者添加 mapper 接口，但此时无法扫描到xml
        // configuration.addMapper(UserMapper.class);
        
        /*
        // 执行初始化sql脚本，若需要执行的话
        Connection connection = sqlSession.getConnection();
        String script = IoUtil.read(getClass().getClassLoader().getResourceAsStream("script/init-mysql.sql"), StandardCharsets.UTF_8);
        MagicUtils.exeScript(script, connection);
        */

        SqlSessionFactory sqlSessionFactory = configuration.build();
        return sqlSessionFactory;
    }

    @Bean
    public MapperManage mapperManage(@Inject SqlSessionFactory sqlSessionFactory) {
        SolonMapperManage manage = new SolonMapperManage(
                sqlSessionFactory.getConfiguration(),
                sqlSessionFactory.openSession());
        manage.setIdGenerate(new DefaultIdGenerate());
        return manage;
    }
}
```

## 3. 直接执行sql

查询（符合mybatis入参语法）
```java
QueryWrapper<UserEntity> query = mapperManage.createQuery(
        "select * from t_user where id>#{id}",
        UserEntity.class);
query.addParam("id", 2);
List<UserEntity> list = query.getList();
System.out.println(list);
```

更新（符合mybatis入参语法）
```java
UpdateWrapper update = mapperManage.createUpdate("update t_user set password=#{p} where id=#{id}");
update.addParam("p", System.currentTimeMillis()).addParam("id", 1);
int execute = update.execute();
log.info("受影响行数: {}", execute);
```

插入（符合mybatis入参语法）
```java
UpdateWrapper update = mapperManage.createUpdate("insert into t_user(id,username) values (2,#{un})");
update.addParam("un", "lk");
int execute = update.execute();
log.info("受影响行数: {}", execute);
```

## 4. 实体对象操作

实体类单表

```java
import lombok.Data;
import top.lingkang.mm.annotation.*;
import java.util.Date;

@Data
@Table("t_user") // t_user 表
public class UserEntity {
    @Id
    private String id;
    private String username;
    private String password;
    private String nickname;
    private Date createTime;
    private Date updateTime;
}
```

增删查改

```java
@Inject
MapperManage mapperManage;

// 查
List<UserEntity> userEntities = mapperManage.selectAll(UserEntity.class);

// 更新
mapperManage.updateById(user);

// 插入
mapperManage.insert(user);

// 删除
mapperManage.deleteById(user);
mapperManage.deleteById(UserEntity.class, 1);

// 是否存在
mapperManage.existsById(UserEntity.class, 1);
```

## 5. 使用mapper.xml

编写`UserMapper`接口
```java
// 不用xml加载时，可以添加 @MagicMapper 注解，就会将此 mapper 接口注入到 bean 中
@MagicMapper
public interface UserMapper extends BaseMapper<UserEntity> {
}
```
编写`mapper/UserMapper.xml`配置，配置中需要添加目录扫描：<br/>
`configuration.addMapperXml("mapper");`或xml文件加载`configuration.addMapperXml("mapper/UserMapper.xml");`
```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "https://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="top.lingkang.mmt.mapper.UserMapper">
    <select id="all" resultType="top.lingkang.mmt.entity.UserEntity">
        select * from t_user
    </select>
</mapper>
```
或者在配置中直接添加`UserMapper`接口
```java
configuration.addMapper(UserMapper.class, sqlSessionFactory.openSession());

// 注入到bean中
@Bean
public UserMapper userMapper(@Inject SqlSessionFactory sqlSessionFactory) {
    org.apache.ibatis.session.Configuration configuration = sqlSessionFactory.getConfiguration();
    if (!configuration.hasMapper(UserMapper.class)) {// 不存在时添加
        configuration.addMapper(UserMapper.class);
    }
    return configuration.getMapper(UserMapper.class, sqlSessionFactory.openSession());
}
```

调用
```java
// @Inject
// private UserMapper userMapper;

// 获取 UserMapper.xml映射的接口，返回的接口对象与mybatis用法一致。
UserMapper mapper = mapperManage.getMapper(UserMapper.class);

// BaseMapper 能力
List<UserEntity> list = mapper.selectAll();
System.out.println(list);

List<UserEntity> all = mapper.all();
System.out.println(all);
```

## 6. 分页

```java
// 开始分页
PageHelper.startPage(1, 5);
// 查询
List<UserEntity> list = mapperManage.selectAll(UserEntity.class);
// 获取到分页的内容
PageInfo page = PageHelper.getPage();

// 开始分页
PageHelper.startPage(1, 10);
UserMapper userMapper = mapperManage.getMapper(UserMapper.class);// mapper 映射
List<UserEntity> list = userMapper.selectAll();
// 获取到分页的内容
PageInfo page = PageHelper.getPage();
```
`默认支持 mysql语法分页，其他数据库分页支持需要定制 PageHelper.pageSqlHandle 属性的实现处理`。支持的数据库如下：
`mysql`、`pgsql`、`h2`、`sqlite`、`sqlserver`

## 7. 事务

* 方式一：在solon的bean生命周期中使用 `@Transactional` 注解，与spring类似。

* 方式二：手动事务：
```java
// 开始事务
TransactionManage.beginTransaction();
try {
    mapperManage.insert(entity, false);
    // 提交事务
    TransactionManage.commit();
} catch (Exception e) {
    // 回滚事务
    TransactionManage.rollback();
    throw e;
}
```