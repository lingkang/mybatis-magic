package test.solon.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;
import test.solon.mapper.UserMapper;
import top.lingkang.mm.MagicConfiguration;
import top.lingkang.mm.gen.DefaultIdGenerate;
import top.lingkang.mm.orm.MapperManage;
import top.lingkang.mm.solon.SolonMapperManage;

import javax.sql.DataSource;

/**
 * @author lingkang
 * Created by 2024/2/28
 */
@Configuration
public class AppConfig {
    @Bean
    public DataSource dataSource() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:mysql://localhost:3306/test?useSSL=true&serverTimezone=UTC");
        config.setUsername("root");
        config.setPassword("123456");
        config.setMaximumPoolSize(1);
        return new HikariDataSource(config);
    }

    @Bean
    public SqlSessionFactory sqlSessionFactory(@Inject DataSource dataSource) {
        MagicConfiguration magicConfiguration = new MagicConfiguration(dataSource);
        magicConfiguration.setMapUnderscoreToCamelCase(true);// 下划线转驼峰
        // 或 xml 文件加载`configuration.addMapperXml("mapper/UserMapper.xml");`
        magicConfiguration.addMapperXml("mapper"); // resources/mapper/UserMapper.xml

        /*
        // 执行初始化sql脚本，若需要执行的话
        Connection connection = sqlSession.getConnection();
        String script = IoUtil.read(getClass().getClassLoader().getResourceAsStream("script/init-mysql.sql"), StandardCharsets.UTF_8);
        MagicUtils.exeScript(script, connection);
        */

        SqlSessionFactory sqlSessionFactory = magicConfiguration.build();
        return sqlSessionFactory;
    }

    @Bean
    public MapperManage mapperManage(@Inject SqlSessionFactory sqlSessionFactory) {
        SolonMapperManage manage = new SolonMapperManage(
                sqlSessionFactory.getConfiguration(),
                sqlSessionFactory.openSession());
        manage.setIdGenerate(new DefaultIdGenerate());
        return manage;
    }
}
