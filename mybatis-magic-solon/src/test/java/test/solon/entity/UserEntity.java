package test.solon.entity;

import lombok.Data;
import top.lingkang.mm.annotation.Id;
import top.lingkang.mm.annotation.PreUpdate;
import top.lingkang.mm.annotation.Table;

import java.util.Date;

/**
 * @author lingkang
 * Created by 2024/2/28
 */
@Table("t_user")
@Data
public class UserEntity {
    @Id
    private String id;
    private String username;
    private String password;
    private String nickname;
    private Date createTime;
    private Date updateTime;

    @PreUpdate
    public void pre(){
        updateTime=new Date();
    }
}
