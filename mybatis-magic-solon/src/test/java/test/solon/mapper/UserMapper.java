package test.solon.mapper;

import test.solon.entity.UserEntity;
import top.lingkang.mm.annotation.MagicMapper;
import top.lingkang.mm.orm.BaseMapper;

import java.util.List;

/**
 * @author lingkang
 * Created by 2024/2/28
 */
@MagicMapper
public interface UserMapper extends BaseMapper<UserEntity> {
    List<UserEntity> all();
}
