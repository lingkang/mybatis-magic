package test.solon.controller;

import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import test.solon.entity.UserEntity;
import test.solon.mapper.UserMapper;
import test.solon.service.UserService;
import top.lingkang.mm.orm.MapperManage;
import top.lingkang.mm.orm.QueryWrapper;
import top.lingkang.mm.solon.Transactional;

import java.util.List;

/**
 * @author lingkang
 * Created by 2024/2/28
 */
@Controller
public class WebController {
    @Inject
    private MapperManage mapperManage;
    @Inject
    private UserMapper userMapper;
    @Inject
    private UserService userService;

    @Mapping("/")
    public Object index() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        System.out.println(mapper.selectAll());
        return mapperManage.getMapper(UserMapper.class).all();
    }

    @Transactional
    @Mapping("/t")
    public Object t() {
        List<UserEntity> list = mapperManage.selectAll(UserEntity.class);
        UserEntity entity = list.get(0);
        entity.setUsername("Transactional");
        mapperManage.updateById(entity);
        if (1 == 1)
            throw new RuntimeException("事务回滚: ");
        return 1;
    }

    @Mapping("/2")
    public Object test2() {
        QueryWrapper<UserEntity> query = mapperManage.createQuery("select * from t_user", UserEntity.class);
        return query.getList();
    }

    @Mapping("/3")
    public Object test3() {
        return userMapper.all();
    }

    @Transactional
    @Mapping("/4")
    public Object test4() {
        return userService.getAll();
    }
}
