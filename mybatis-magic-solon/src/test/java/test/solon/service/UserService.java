package test.solon.service;

import test.solon.entity.UserEntity;

import java.util.List;

/**
 * @author lingkang
 * @create by 2024/6/24 9:45
 */
public interface UserService {
    List<UserEntity> getAll();
}
