package test.solon.service.impl;

import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;
import test.solon.entity.UserEntity;
import test.solon.mapper.UserMapper;
import test.solon.service.UserService;
import top.lingkang.mm.solon.Transactional;

import java.util.List;

/**
 * @author lingkang
 * @create by 2024/6/24 9:46
 */
@Component
public class UserServiceImpl implements UserService {
    @Inject
    private UserMapper userMapper;

    @Transactional
    @Override
    public List<UserEntity> getAll() {
        return userMapper.selectAll();
    }
}
