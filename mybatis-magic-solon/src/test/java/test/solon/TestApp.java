package test.solon;

import org.noear.solon.Solon;

/**
 * @author lingkang
 * @create by 2024/3/4 15:34
 */
public class TestApp {
    public static void main(String[] args) {
        Solon.start(TestApp.class, args, solonApp -> {
            solonApp.enableTransaction(false);
        });
    }
}
