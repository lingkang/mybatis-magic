package top.lingkang.mm.solon;

import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import top.lingkang.mm.MagicSqlSession;
import top.lingkang.mm.error.MagicException;
import top.lingkang.mm.orm.MapperManageImpl;

/**
 * @author lingkang
 * @create by 2024/3/4 16:14
 */
public class SolonMapperManage extends MapperManageImpl {
    public SolonMapperManage(Configuration configuration, SqlSession sqlSession) {
        super(configuration, sqlSession);
        if (!(sqlSession instanceof MagicSqlSession)) {
            throw new MagicException("solon 中配置的 SqlSession 必须是 top.lingkang.mm.MagicSqlSession 类");
        }
    }
}
