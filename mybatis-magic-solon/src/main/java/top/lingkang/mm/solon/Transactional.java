package top.lingkang.mm.solon;

import java.lang.annotation.*;

/**
 * 启用事务注解，若当前已经存在事务，则加入事务
 *
 * @author lingkang
 * @create by 2024/3/4 15:52
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Transactional {
}
