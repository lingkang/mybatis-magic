## 1. mybatis-magic

`mybatis`能力扩展框架，兼顾`mybatis`的`mapper.xml`编写操作数据库。

## 2. 用法

### 2.1 依赖

```html
<dependency>
    <groupId>top.lingkang</groupId>
    <artifactId>mybatis-magic</artifactId>
    <version>1.1.2</version>
</dependency>
<!-- https://mvnrepository.com/artifact/mysql/mysql-connector-java -->
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>8.0.33</version>
</dependency>
<!-- https://mvnrepository.com/artifact/com.zaxxer/HikariCP -->
<dependency>
    <groupId>com.zaxxer</groupId>
    <artifactId>HikariCP</artifactId>
    <version>4.0.3</version>
</dependency>
```
快照版: [点此查看如何使用快照先行版](https://gitcode.com/lingkang/mybatis-magic/blob/main/doc/10.%E5%BF%AB%E7%85%A7%E7%89%88%E6%9C%AC.md)

### 2.2 配置
```java
// 配置数据库连接
HikariConfig config = new HikariConfig();
config.setJdbcUrl("jdbc:mysql://localhost:3306/test?useSSL=true");
config.setUsername("root");
config.setPassword("123456");
config.setMaximumPoolSize(1);
HikariDataSource dataSource = new HikariDataSource(config);

// 配置mybatis
MagicConfiguration configuration = new MagicConfiguration(dataSource);
// 推荐开启
configuration.setMapUnderscoreToCamelCase(true);// 下划线转驼峰

// 文件加载 resources/mapper/UserMapper.xml
configuration.addMapperXml("mapper/UserMapper.xml");
// 或者
// configuration.addMapperXml("mapper/**.xml");

// 直接者添加 mapper 接口，但此时无法扫描到xml
// configuration.addMapper(UserMapper.class);

SqlSessionFactory sessionFactory = configuration.build();
SqlSession sqlSession = sessionFactory.openSession();

/*
// 执行初始化sql脚本，若需要执行的话
Connection connection = sqlSession.getConnection();
String script = IoUtil.read(getClass().getClassLoader().getResourceAsStream("script/init-mysql.sql"), StandardCharsets.UTF_8);
MagicUtils.exeScript(script, connection);
*/

// mybatis-magic 的增删查改
MapperManage mapperManage = new MapperManageImpl(configuration, sqlSession);
```

### 2.3 直接执行sql

查询（符合mybatis入参语法）
```java
QueryWrapper<UserEntity> query = mapperManage.createQuery(
        "select * from t_user where id>#{id}",
        UserEntity.class);
query.addParam("id", 2);
List<UserEntity> list = query.getList();
System.out.println(list);
```

更新（符合mybatis入参语法）
```java
UpdateWrapper update = mapperManage.createUpdate("update t_user set password=#{p} where id=#{id}");
update.addParam("p", System.currentTimeMillis()).addParam("id", 1);
int execute = update.execute();
log.info("受影响行数: {}", execute);
```

插入（符合mybatis入参语法）
```java
UpdateWrapper update = mapperManage.createUpdate("insert into t_user(id,username) values (2,#{un})");
update.addParam("un", "lk");
int execute = update.execute();
log.info("受影响行数: {}", execute);
```

### 2.4 实体对象操作

实体类
```java
import lombok.Data;
import top.lingkang.mm.annotation.*;
import java.util.Date;

@Data
@Table("t_user") // t_user 表
public class UserEntity {
    @Id
    private String id;
    private String username;
    private String password;
    private String nickname;
    private Date createTime;
    private Date updateTime;
}
```

增删查改
```java
// 查
List<UserEntity> userEntities = mapperManage.selectAll(UserEntity.class);

// 更新
mapperManage.updateById(user);

// 插入
mapperManage.insert(user);

// 删除
mapperManage.deleteById(user);
mapperManage.deleteById(UserEntity.class, 1);

// 是否存在
mapperManage.existsById(UserEntity.class, 1);
```

### 2.5 使用mapper.xml

编写`UserMapper`接口
```java
public interface UserMapper extends BaseMapper {

    List<UserEntity> all();
}
```
编写`mapper/UserMapper.xml`配置，配置中需要添加目录扫描：<br/>
`configuration.addMapperXml("mapper");`或xml文件加载`configuration.addMapperXml("mapper/UserMapper.xml");`
```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "https://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="top.lingkang.mmt.mapper.UserMapper">
    <select id="all" resultType="top.lingkang.mmt.entity.UserEntity">
        select * from t_user
    </select>
</mapper>
```
或者在配置中直接添加`UserMapper`接口
```java
configuration.addMapper(UserMapper.class);
```

调用
```java
// 获取 UserMapper.xml映射的接口，返回的接口对象与mybatis用法一致。
UserMapper mapper = mapperManage.getMapper(UserMapper.class);

// BaseMapper 能力
List<UserEntity> list = mapper.selectAll();
System.out.println(list);

List<UserEntity> all = mapper.all();
System.out.println(all);
```

### 2.6 分页

```java
// 开始分页
PageHelper.startPage(1, 5);
// 查询
List<UserEntity> list = mapperManage.selectAll(UserEntity.class);
// 获取到分页的内容
PageInfo page = PageHelper.getPage();

// 开始分页
PageHelper.startPage(1, 10);
UserMapper userMapper = mapperManage.getMapper(UserMapper.class);// mapper 映射
List<UserEntity> list = userMapper.selectAll();
// 获取到分页的内容
PageInfo page = PageHelper.getPage();
```
`默认支持 mysql语法分页，其他数据库分页支持需要定制 PageHelper.pageSqlHandle 属性的实现处理`。支持的数据库如下：
`mysql`、`pgsql`、`h2`、`sqlite`、`sqlserver`

### 2.7 事务

```java
// 开始事务
TransactionManage.beginTransaction();
try {
    mapperManage.insert(entity, false);
    // 提交事务
    TransactionManage.commit();
} catch (Exception e) {
    // 回滚事务
    TransactionManage.rollback();
    throw e;
}
```

### 2.8 打印日志
打印对应`mapper`、`dao`的包名即可。
```xml
<appender name="stdout" class="ch.qos.logback.core.ConsoleAppender">
    <encoder>
        <pattern>%d{"yyyy-MM-dd HH:mm:ss.SSS"} %highlight(%level) [%thread] %cyan(%logger{0}) %L: %msg%n</pattern>
    </encoder>
</appender>

<!-- 为特定的包设置 DEBUG 级别 -->
<!-- 打印 实体Mapper 日志 -->
<logger name="top.lingkang.xxxx.mapper" level="DEBUG" additivity="false">
    <!-- 将日志输出到控制台 -->
    <appender-ref ref="stdout"/>
</logger>
<!-- 打印 MapperManage 接口操作日志 -->
<logger name="top.lingkang.mm.orm" level="DEBUG" additivity="false">
    <appender-ref ref="stdout"/>
</logger>
```

## 3. springboot 支持

用法请查看此链接：
[https://gitcode.com/lingkang/mybatis-magic/blob/main/mybatis-magic-spring-boot-starter/README.md](https://gitcode.com/lingkang/mybatis-magic/blob/main/mybatis-magic-spring-boot-starter/README.md)

## 4. solon 支持

用法请查看此链接：
[https://gitcode.com/lingkang/mybatis-magic/blob/main/mybatis-magic-solon/README.md](https://gitcode.com/lingkang/mybatis-magic/blob/main/mybatis-magic-solon/README.md)

## 5. 测试用例覆盖

| 项目            | 覆盖率  |
|---------------|------|
| 原生java       | 100% |
| solon         | 100% |
| springboot    | 80%  |

## 数据库支持情况

| 数据库        | 覆盖率  |
|------------|------|
| mysql      | 100% |
| sqlite     | 100% |
| h2         | 100% |
| postgresql | 100% |

`100%` 通过 `100+` 测试用例
