# mybatis-magic-spring-boot-starter

`mybatis-magic` 的 `spring` 实现。

## 1. 依赖

`springboot 2.x +`

```xml
<dependency>
    <groupId>top.lingkang</groupId>
    <artifactId>mybatis-magic-spring-boot-starter</artifactId>
    <version>1.1.2</version>
</dependency>
```
`自带了 mybatis-spring-boot-starter 依赖`

## 2. 配置

`application.properties`
```properties
# 推荐开启下划线转换
mybatis.configuration.map-underscore-to-camel-case=true
# 扫描的 xml
mybatis.mapper-locations=mapper/*.xml
```

Bean 配置：`默认自动配置`，您也可以手动配置

```java
@Bean
public MapperManage mapperManage(@Autowired SqlSessionFactory sqlSessionFactory,
                                 @Autowired SqlSessionTemplate sessionTemplate) {
    SpringMapperManage manage = new SpringMapperManage(sqlSessionFactory.getConfiguration(), sessionTemplate);
    // 添加实体属性反射处理，推荐替换
    sqlSessionFactory.getConfiguration().setReflectorFactory(new MagicReflectorFactory());
    // 添加分页处理，使用分页，必须添加
    sqlSessionFactory.getConfiguration().addInterceptor(new MagicPageInterceptor());
    manage.setIdGenerate(new DefaultIdGenerate());
    return manage;
}
```

## 3. 直接执行sql

查询（符合mybatis入参语法）
```java
QueryWrapper<UserEntity> query = mapperManage.createQuery(
        "select * from t_user where id>#{id}",
        UserEntity.class);
query.addParam("id", 2);
List<UserEntity> list = query.getList();
System.out.println(list);
```

更新（符合mybatis入参语法）
```java
UpdateWrapper update = mapperManage.createUpdate("update t_user set password=#{p} where id=#{id}");
update.addParam("p", System.currentTimeMillis()).addParam("id", 1);
int execute = update.execute();
log.info("受影响行数: {}", execute);
```

插入（符合mybatis入参语法）
```java
UpdateWrapper update = mapperManage.createUpdate("insert into t_user(id,username) values (2,#{un})");
update.addParam("un", "lk");
int execute = update.execute();
log.info("受影响行数: {}", execute);
```

## 4. 实体对象操作

实体类单表

```java
import lombok.Data;
import top.lingkang.mm.annotation.*;
import java.util.Date;

@Data
@Table("t_user") // t_user 表
public class UserEntity {
    @Id
    private String id;
    private String username;
    private String password;
    private String nickname;
    private Date createTime;
    private Date updateTime;
}
```

增删查改

```java
@Autowired
private MapperManage mapperManage;

// 查
List<UserEntity> userEntities = mapperManage.selectAll(UserEntity.class);

// 更新
mapperManage.updateById(user);

// 插入
mapperManage.insert(user);

// 删除
mapperManage.deleteById(user);
mapperManage.deleteById(UserEntity.class, 1);

// 是否存在
mapperManage.existsById(UserEntity.class, 1);
```

## 5. BaseMapper 能力

```java
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper extends BaseMapper<UserEntity> {
    List<UserEntity> all();
}
```
也可以添加`mapper/UserMapper.xml`:
```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "https://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="spring.test.mapper.UserMapper">
    <select id="all" resultType="spring.test.entity.UserEntity">
        select * from t_user
    </select>
</mapper>
```

调用
```java
@Autowired
private UserMapper userMapper;

// BaseMapper 能力
List<UserEntity> list = userMapper.selectAll();
System.out.println(list);
```

## 6. 分页

```java
// 开始分页
PageHelper.startPage(1, 5);
// 查询
List<UserEntity> list = mapperManage.selectAll(UserEntity.class);
// 获取到分页的内容
PageInfo page = PageHelper.getPage();

// 开始分页
PageHelper.startPage(1, 10);
UserMapper userMapper = mapperManage.getMapper(UserMapper.class);// mapper 映射
List<UserEntity> list = userMapper.selectAll();
// 获取到分页的内容
PageInfo page = PageHelper.getPage();
```
`默认支持 mysql语法分页，其他数据库分页支持需要定制 PageHelper.pageSqlHandle 属性的实现处理`。支持的数据库如下：
`mysql`、`pgsql`、`h2`、`sqlite`、`sqlserver`

## 7. 事务

使用`spring`的`@Transaction`注解开启。

`mybatis-magic`的`TransactionManage`无法使用，事务已经交由`springboot-mybatis`管理，底层由`mybatis-spring`实现。
