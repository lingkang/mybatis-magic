package spring.test.mapper;

import org.apache.ibatis.annotations.Mapper;
import spring.test.entity.UserEntity;
import top.lingkang.mm.orm.BaseMapper;

import java.util.List;

/**
 * @author lingkang
 * @create by 2024/3/4 11:30
 */
@Mapper
public interface UserMapper extends BaseMapper<UserEntity> {
    List<UserEntity> all();
}
