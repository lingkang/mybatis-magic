package spring.test.mapper;

import org.apache.ibatis.annotations.Mapper;
import spring.test.entity.OrderEntity;
import top.lingkang.mm.orm.BaseMapper;

/**
 * @author lingkang
 * @create by 2024/3/13 16:03
 */
@Mapper
public interface OrderMapper extends BaseMapper<OrderEntity> {
}
