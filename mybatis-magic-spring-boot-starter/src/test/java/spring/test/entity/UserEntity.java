package spring.test.entity;

import lombok.Data;
import top.lingkang.mm.annotation.*;

import java.util.Date;

/**
 * @author lingkang
 * @create by 2024/3/4 11:27
 */
@Data
@Table("t_user")
public class UserEntity {
    @Id
    private String id;
    private String username;
    private String password;
    private String nickname;
    private Date createTime;
    private Date updateTime;
}
