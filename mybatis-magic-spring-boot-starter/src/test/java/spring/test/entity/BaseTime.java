package spring.test.entity;

import lombok.Data;
import top.lingkang.mm.annotation.PostUpdate;
import top.lingkang.mm.annotation.PreUpdate;

import java.util.Date;

/**
 * @author lingkang
 * @create by 2024/3/6 17:13
 */
@Data
public class BaseTime {
    protected Date createTime;
    protected Date updateTime;

    @PostUpdate
    @PreUpdate
    public void postUpdate() {
        System.out.println(updateTime);
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

}
