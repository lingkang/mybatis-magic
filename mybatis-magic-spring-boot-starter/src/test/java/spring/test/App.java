package spring.test;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import spring.test.entity.OrderEntity;
import spring.test.entity.UserEntity;
import spring.test.mapper.OrderMapper;
import spring.test.mapper.UserMapper;
import top.lingkang.mm.orm.MapperManage;
import top.lingkang.mm.orm.QueryWrapper;
import top.lingkang.mm.page.PageHelper;
import top.lingkang.mm.page.PageInfo;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lingkang
 * @create by 2024/3/4 11:26
 */
@RestController
@SpringBootApplication
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @Autowired
    private MapperManage mapperManage;
    @Autowired
    private SqlSessionTemplate sqlSessionTemplate;
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private UserMapper userMapper;

    @GetMapping("/")
    public Object index() {
        // 查
        List<UserEntity> userEntities = mapperManage.selectAll(UserEntity.class);
        UserEntity user = userEntities.get(0);

        // 更新
        mapperManage.updateById(user);

        // 插入
        // mapperManage.insert(user);

        // 删除
        mapperManage.deleteById(user);
        return "" + userEntities;
    }

    @GetMapping("/1")
    public Object t1() {
        Method[] methods = sqlSessionTemplate.getClass().getDeclaredMethods();
        for (Method method : methods)
            System.out.println(method.getName());
        List<Object> list = sqlSessionTemplate.selectList("selectAll");
        return list;
    }

    @GetMapping("/2")
    public Object t2() {
        QueryWrapper<UserEntity> query = mapperManage.createQuery("select * from t_user", UserEntity.class);
        List<UserEntity> list = query.getList();
        return list;
    }

    @GetMapping("/22")
    public Object t22() {
        QueryWrapper<UserEntity> query = mapperManage.createQuery("select * from t_user where id=1231", UserEntity.class);
        List<UserEntity> list = query.getList();
        return list;
    }

    @GetMapping("/3")
    public Object t3() {
        List<OrderEntity> list = orderMapper.selectAll();
        return list;
    }

    @GetMapping("/4")
    public Object t4() {
        OrderEntity list = orderMapper.selectById("1");
        return list;
    }


    @GetMapping("/5")
    public Object t5() {
        UserEntity entity = userMapper.selectById(1);
        return entity;
    }

    @GetMapping("/6")
    public Object t6() {
        UserMapper mapper = mapperManage.getMapper(UserMapper.class);
        System.out.println(mapper.all());
        List<UserEntity> userEntities = userMapper.selectAll();
        return userEntities;
    }

    @GetMapping("/all")
    public Object all() {
        return userMapper.all();
    }

    @GetMapping("/page")
    public Object page() {
        PageHelper.startPage(1, 10);
        List<UserEntity> all = userMapper.all();
        PageInfo page = PageHelper.getPage();
        Map<String, Object> result = new HashMap<>();
        result.put("data", all);
        result.put("page", page);
        return result;
    }

    @GetMapping("/page2")
    public Object page2() {
        PageHelper.startPage(1, 10);
        List<UserEntity> all = userMapper.selectAll();
        PageInfo page = PageHelper.getPage();
        Map<String, Object> result = new HashMap<>();
        result.put("data", all);
        result.put("page", page);
        return result;
    }

    @Transactional
    @GetMapping("/tran")
    public Object tran(){
        List<UserEntity> list = userMapper.selectAll();
        if (!list.isEmpty()){
            UserEntity entity = list.get(0);
            entity.setUsername("tran");
            userMapper.updateById(entity);
            if (1==1)
                throw new RuntimeException("1==1");
        }
        return "ok";
    }

}
