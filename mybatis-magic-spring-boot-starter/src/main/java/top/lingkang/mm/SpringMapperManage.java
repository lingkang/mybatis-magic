package top.lingkang.mm;

import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.lingkang.mm.error.MagicException;
import top.lingkang.mm.orm.MagicEntity;
import top.lingkang.mm.orm.MapperManageImpl;

/**
 * @author lingkang
 * @create by 2024/3/4 12:27
 */
public class SpringMapperManage extends MapperManageImpl {
    private static final Logger log = LoggerFactory.getLogger(SpringMapperManage.class);

    public SpringMapperManage(Configuration configuration, SqlSession sqlSession) {
        super(configuration, sqlSession);
        if (!(sqlSession instanceof SqlSessionTemplate)){
            throw new MagicException("springboot 中配置的 SqlSession 必须是 org.mybatis.spring.SqlSessionTemplate 类");
        }
    }

    @Override
    protected void loadTemplate(MagicEntity entity) {
        try {
            super.loadTemplate(entity);
        } catch (Exception e) {
            // spring 中忽略加载异常
            log.debug(e.getMessage());
        }
    }
}
